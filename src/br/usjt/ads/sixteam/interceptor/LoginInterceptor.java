package br.usjt.ads.sixteam.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class LoginInterceptor extends HandlerInterceptorAdapter {
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object controller) throws Exception {
		
		String uri = request.getRequestURI();
		
		if (uri.endsWith("/") || uri.endsWith("inicio") || uri.endsWith("fazer_login") || uri.endsWith("tela_login") || uri.endsWith("fazer_logout") || uri.endsWith("campeonatos") || uri.contains("equipes") || uri.contains("jogadores") || uri.contains("jogos") || uri.contains("treinadores") || uri.contains("arbitros") || uri.contains("visualizar") || uri.contains("selecao") || uri.contains("css") || uri.contains("js") ||uri.contains("img")) {
			return true;
		}
		
		if (request.getSession().getAttribute("usuario") != null) {
			return true;
		}
		response.sendRedirect("inicio");
		return false;
	}
	
}

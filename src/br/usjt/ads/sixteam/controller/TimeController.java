package br.usjt.ads.sixteam.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.usjt.ads.sixteam.model.entity.Historico;
import br.usjt.ads.sixteam.model.entity.Time;
import br.usjt.ads.sixteam.model.entity.Usuario;
import br.usjt.ads.sixteam.model.service.CategoriaService;
import br.usjt.ads.sixteam.model.service.HistoricoService;
import br.usjt.ads.sixteam.model.service.TecnicoService;
import br.usjt.ads.sixteam.model.service.TimeService;

@Controller
public class TimeController {
	
	@Autowired
	private HistoricoService historicoService;
	@Autowired
	private TimeService timeService;
	@Autowired
	private TecnicoService tecnicoService;
	@Autowired
	private CategoriaService categoriaService;
	
	/*
	 ==============================================
	 =============Formata��o de Data===============
	 ==============================================
	*/
	@InitBinder
	public void initBinder(WebDataBinder binder) {
	    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
	    dateFormat.setLenient(false);
	    binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
	}
	
	/*
	 ==============================================
	 ====================TELA======================
	 ==============Cadastrar Equipe================
	 ==============================================
	*/
	@RequestMapping("/equipe/cadastro")
 	public String novaEquipe(HttpSession session) {
		session.setAttribute("timeExistente", null);
		try {
			session.setAttribute("categorias", categoriaService.buscarTodas());
			session.setAttribute("tecnicos", tecnicoService.buscarTodos());
			return "equipe/nova-equipe";
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "redirect:/equipes";
	}
	
	/*
	 ==============================================
	 ====================TELA======================
	 ================Editar Equipe================
	 ==============================================
	*/
	@RequestMapping("/equipes/editar/{id}")
	public String telaEditarEquipe(@PathVariable("id") int id, HttpSession session) {
		session.setAttribute("timeExistente", null);
		try {
			Time time = timeService.buscar(id);
			session.setAttribute("time", time);
			session.setAttribute("categorias", categoriaService.buscarTodas());
			session.setAttribute("tecnicos", tecnicoService.buscarTodos());
			return "equipe/editar-equipe";
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "redirect:/equipes";
	}
	
	/*
	 ==============================================
	 ====================TELA======================
	 ===============Lista de Equipes===============
	 ==============================================
	*/
	@RequestMapping("/equipes")
	public String equipes(HttpSession session) {
		List<Time> listaTime;
		try {
			listaTime = timeService.buscarTodos();
			session.setAttribute("listaTime", listaTime);
			return "/equipe/lista-equipes";
		} catch (IOException e) {
			session.setAttribute("lista", null);
			e.printStackTrace();
		}
		return "redirect:/inicio";
	}
	
	// ========================================== CRUD ================================================================================
	
	/*
	 ==============================================
	 ===================CREATE=====================
	 ==============================================
	*/
	@RequestMapping("/inserir_equipe")
	public String inserirEquipe(@Valid Time time, BindingResult result, HttpSession session) {
		Usuario usuario = (Usuario) session.getAttribute("usuario");
		try {
			if (timeService.isTimeCadastrado(time.getNome())) {
				session.setAttribute("timeExistente", time.getNome());
				return "equipe/nova-equipe";
			}
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		try {
			if(!result.hasFieldErrors()) {
				int idTime = timeService.inserir(time);
				historicoService.inserir(new Historico("Equipe", "Inclus�o #" + idTime, usuario.getId()));
				return "redirect:/equipes";
			} else {
				return "equipe/nova-equipe";
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "redirect:/inicio";
	}
	
	/*
	 ==============================================
	 ===================UPDATE=====================
	 ==============================================
	*/
	@RequestMapping("/atualizar_equipe")
	public String atualizarEquipe(@Valid Time time, BindingResult result, HttpSession session) {
		Usuario usuario = (Usuario) session.getAttribute("usuario");
		try {
			if(!result.hasFieldErrors()) {
				timeService.atualizar(time);
				historicoService.inserir(new Historico("Equipe", "Atualiza��o #" + time.getId(), usuario.getId()));
				return "redirect:/equipes";
			} else {
				return "equipe/editar-equipe";
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "redirect:/inicio";
	}
	
	/*
	 ==============================================
	 ===================DELETE=====================
	 ==============================================
	*/
	@RequestMapping("/equipe/desativar")
	public String excluirEquipe(@RequestParam("id") int id, HttpSession session) {
		Usuario usuario = (Usuario) session.getAttribute("usuario");
		try {
			timeService.excluir(id);
			historicoService.inserir(new Historico("Equipe", "Exclus�o #" + id, usuario.getId()));
			return "redirect:/equipes";
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "redirect:/inicio";
	}
}

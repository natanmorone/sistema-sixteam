package br.usjt.ads.sixteam.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.usjt.ads.sixteam.model.entity.Historico;
import br.usjt.ads.sixteam.model.entity.Jogador;
import br.usjt.ads.sixteam.model.entity.Usuario;
import br.usjt.ads.sixteam.model.service.CategoriaService;
import br.usjt.ads.sixteam.model.service.HistoricoService;
import br.usjt.ads.sixteam.model.service.JogadorService;

@Controller
public class JogadorController {
	
	@Autowired
	private JogadorService jogadorService;
	@Autowired
	private HistoricoService historicoService;
	@Autowired
	private CategoriaService categoriaService;
	
	/*
	 ==============================================
	 =============Formata��o de Data===============
	 ==============================================
	*/
	@InitBinder
	public void initBinder(WebDataBinder binder) {
	    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
	    dateFormat.setLenient(false);
	    binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
	}
	
	/*
	 ==============================================
	 ====================TELA======================
	 ==============Cadastrar Jogador===============
	 ==============================================
	*/
	@RequestMapping("/jogador/cadastro")
	public String novoJogador(HttpSession session) {
		try {
			session.setAttribute("categorias", categoriaService.buscarTodas());
			return "jogador/novo-jogador";
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "redirect:/jogadores";
	}
	
	/*
	 ==============================================
	 ====================TELA======================
	 ===============Editar Jogador================
	 ==============================================
	*/
	@RequestMapping("/jogadores/editar/{id}")
	public String telaEditarJogador(@PathVariable("id") int id, HttpSession session) {
		try {
			Jogador jogador = jogadorService.buscar(id);
			session.setAttribute("jogador", jogador);
			session.setAttribute("categorias", categoriaService.buscarTodas());
			return "jogador/editar-jogador";
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "redirect:jogadores";
	}
	
	/*
	 ==============================================
	 ====================TELA======================
	 ==============Lista de Jogadores==============
	 ==============================================
	*/
	@RequestMapping("/jogadores")
	public String jogadores(HttpSession session) {
		List<Jogador> jogadores;
		try {
			jogadores = jogadorService.buscarTodos();
			session.setAttribute("jogadores", jogadores);
			return "/jogador/lista-jogadores";
		} catch (IOException e) {
			session.setAttribute("jogadores", null);
			e.printStackTrace();
		}
		return "redirect:/inicio";
	}
	
	
	// ========================================== CRUD ================================================================================
	
	/*
	 ==============================================
	 ===================CREATE=====================
	 ==============================================
	*/
	@RequestMapping("/inserir_jogador")
	public String inserirJogador(@Valid Jogador jogador, BindingResult result, HttpSession session) {
		Usuario usuario = (Usuario) session.getAttribute("usuario");
		try {
			if(!result.hasFieldErrors()) {
				int idJogador = jogadorService.inserir(jogador);
				historicoService.inserir(new Historico("Jogador", "Inclus�o #" + idJogador, usuario.getId()));
				return "redirect:/jogadores";
			} else {
				return "jogador/novo-jogador";
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "redirect:/inicio";
	}
	
	/*
	 ==============================================
	 ===================UPDATE=====================
	 ==============================================
	*/
	@RequestMapping("/atualizar_jogador")
	public String atualizarJogador(@Valid Jogador jogador, BindingResult result, HttpSession session) {
		Usuario usuario = (Usuario) session.getAttribute("usuario");
		try {
			if(!result.hasFieldErrors()) {
				jogadorService.atualizar(jogador);
				historicoService.inserir(new Historico("Jogador", "Atualiza��o #" + jogador.getId(), usuario.getId()));
				return "redirect:/jogadores";
			} else {
				return "jogador/editar-jogador";
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "redirect:/inicio";
	}
	
	/*
	 ==============================================
	 ===================DELETE=====================
	 ==============================================
	*/
	@RequestMapping("/jogador/desativar")
	public String excluirJogador(@RequestParam("id") int id, HttpSession session) {
		Usuario usuario = (Usuario) session.getAttribute("usuario");
		try {
			jogadorService.excluir(id);
			historicoService.inserir(new Historico("Jogador", "Exclus�o #" + id, usuario.getId()));
			return "redirect:/jogadores";
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "redirect:/inicio";
	}
}

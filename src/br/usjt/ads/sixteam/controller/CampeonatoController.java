package br.usjt.ads.sixteam.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.usjt.ads.sixteam.model.entity.Campeonato;
import br.usjt.ads.sixteam.model.entity.Historico;
import br.usjt.ads.sixteam.model.entity.Jogo;
import br.usjt.ads.sixteam.model.entity.TimeCampeonato;
import br.usjt.ads.sixteam.model.entity.Usuario;
import br.usjt.ads.sixteam.model.service.CampeonatoService;
import br.usjt.ads.sixteam.model.service.CategoriaService;
import br.usjt.ads.sixteam.model.service.EstatisticaService;
import br.usjt.ads.sixteam.model.service.GrupoService;
import br.usjt.ads.sixteam.model.service.HistoricoService;
import br.usjt.ads.sixteam.model.service.JogoService;
import br.usjt.ads.sixteam.model.service.RegraService;
import br.usjt.ads.sixteam.model.service.TimeCampeonatoService;

@Controller
public class CampeonatoController {
	
	@Autowired
	private CampeonatoService campeonatoService;
	@Autowired
	private RegraService regraService;
	@Autowired
	private CategoriaService categoriaService;
	@Autowired
	private HistoricoService historicoService;
	@Autowired
	private GrupoService grupoService;
	@Autowired
	private TimeCampeonatoService timeCampeonatoService;
	@Autowired
	private EstatisticaService estatisticaService;
	@Autowired
	private JogoService jogoService;
	
	
	/*
	 ==============================================
	 =============Formata��o de Data===============
	 ==============================================
	*/
	@InitBinder
	public void initBinder(WebDataBinder binder) {
	    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
	    dateFormat.setLenient(false);
	    binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
	}
	
	/*
	 ==============================================
	 ====================TELA======================
	 ============Cadastrar Campeonato==============
	 ==============================================
	*/
	@RequestMapping("/campeonato/cadastro")
	public String novoCampeonato(HttpSession session) {
		try {
			session.setAttribute("categorias", categoriaService.buscarTodas());
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "campeonato/novo-campeonato";
	}
	
	/*
	 ==============================================
	 ====================TELA======================
	 ==============Editar Campeonato===============
	 ==============================================
	*/
	@RequestMapping("/campeonatos/editar/{id}")
	public String telaEditarCampeonato(@PathVariable("id") int id, HttpSession session) {
		try {
			session.setAttribute("categorias", categoriaService.buscarTodas());
			session.setAttribute("campeonato", campeonatoService.buscar(id));
			return "campeonato/editar-campeonato";
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "redirect:campeonatos";
	}
	
	/*
	 ==============================================
	 ====================TELA======================
	 ===========Visualizar Campeonato==============
	 ==============================================
	*/
	@RequestMapping("/campeonatos/visualizar/{id}")
	public String visualizarCampeonato(@PathVariable("id") int id, HttpSession session) {
		try {
			Campeonato campeonato = campeonatoService.buscar(id);
			session.setAttribute("grupos", grupoService.buscarListaGrupos(campeonato.getRegra().getQuantidadeGrupos()));
			List<TimeCampeonato> timesCampeonato = timeCampeonatoService.buscarTodosPorCampeonato(id);
			for ( TimeCampeonato time : timesCampeonato) {
				int pontos = 0, vitorias = 0, empates = 0, empatesZero = 0, derrotas = 0, golsPro = 0, golsContra = 0;
				List<Jogo> jogosDoTime = jogoService.buscarTodosPorEquipe(time);
				List<TimeCampeonato> timesContra = estatisticaService.getTimesContra(time, jogosDoTime);
				for(Jogo jogo : jogosDoTime) {
					if (estatisticaService.isVitoria(time, jogo)) {
						vitorias++;
					}
					if (estatisticaService.isEmpate(jogo)) {
						empates++;
					}
					if (estatisticaService.isEmpateZero(jogo)) {
						empatesZero++;
					}
					golsPro += estatisticaService.buscarGols(time, jogo);
					for (TimeCampeonato timeContra : timesContra) {
						golsContra += estatisticaService.buscarGols(timeContra, jogo);
					}
				}
				
				derrotas = jogosDoTime.size() - vitorias - empates;
				time.setJg(jogosDoTime.size());
				time.setVt(vitorias);
				time.setDt(derrotas);
				time.setEm(empates);
				pontos += vitorias * time.getCampeonato().getRegra().getPontosVitoria();
				pontos += derrotas * time.getCampeonato().getRegra().getPontosDerrota();
				pontos += empates * time.getCampeonato().getRegra().getPontosEmpate();
				pontos += empatesZero * time.getCampeonato().getRegra().getPontosEmpateZero();
				time.setPg(pontos);
				time.setGp(golsPro);
				time.setGc(golsContra);
				time.setSg(golsPro - golsContra);
				time.setCa(estatisticaService.buscarCA(time).size());
				time.setCv(estatisticaService.buscarCV(time).size());
				time.setPd(time.getCa() + (time.getCv() * 3));
			}
			session.setAttribute("timesCampeonato", timesCampeonato);
			session.setAttribute("campeonato", campeonato);
			return "campeonato/visualizar-campeonato";
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "redirect:campeonatos";
	}
	
	/*
	 ==============================================
	 ====================TELA======================
	 =============Lista de Campeonatos=============
	 ==============================================
	*/
	@RequestMapping("/campeonatos")
	public String campeonatos(HttpSession session) throws IOException {
		List<Campeonato> lista;
		try {
			lista = campeonatoService.buscarTodos();
			session.setAttribute("lista", lista);
			return "/campeonato/lista-campeonatos";
		} catch (IOException e) {
			session.setAttribute("lista", null);
			e.printStackTrace();
		}
		return "campeonato/lista-campeonatos";
	}
	
	/*
	 ==============================================
	 ====================TELA======================
	 =============Sele��o Campeonato===============
	 ==============================================
	*/
	@RequestMapping("/selecao_campeonato")
	public String selecaoCampeonato(@RequestParam("tipo") String tipo, HttpSession session) {
		List<Campeonato> lista;
		try {
			lista = campeonatoService.buscarTodos();
			session.setAttribute("lista", lista);
			session.setAttribute("tipo", tipo);
			return "campeonato/selecao-campeonato";
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return "redirect:/";
		
	}
	
	// ========================================== CRUD ================================================================================
	
	/*
	 ==============================================
	 ===================CREATE=====================
	 ==============================================
	*/
	@RequestMapping("/inserir_campeonato")
	public String inserirCampeonato(@Valid Campeonato campeonato, BindingResult result, HttpSession session) {
		Usuario usuario = (Usuario) session.getAttribute("usuario");
		try {
			if(!result.hasFieldErrors()) {
				int idRegra = regraService.inserir(campeonato.getRegra());
				campeonato.getRegra().setId(idRegra);
				historicoService.inserir(new Historico("Regra", "Inclus�o #" + idRegra, usuario.getId()));
				int idCampeonato = campeonatoService.inserir(campeonato);
				historicoService.inserir(new Historico("Campeonato", "Inclus�o #" + idCampeonato, usuario.getId()));
				return "redirect:campeonatos";
			} else {
				return "campeonato/novo-campeonato";
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "index";
	}
	
	/*
	 ==============================================
	 ===================UPDATE=====================
	 ==============================================
	*/
	@RequestMapping("/atualizar_campeonato")
	public String atualizarCampeonato(@Valid Campeonato campeonato, BindingResult result, HttpSession session) {
		Usuario usuario = (Usuario) session.getAttribute("usuario");
		int idCampeonato = campeonato.getId();
		
		try {
			if(!result.hasFieldErrors()) {
				regraService.atualizar(campeonato.getRegra());
				historicoService.inserir(new Historico("Regra", "Atualiza��o #" + campeonato.getRegra().getId(), usuario.getId()));
				campeonato.setCategoria(categoriaService.buscar(campeonato.getCategoria().getId()));
				campeonatoService.atualizar(campeonato);
				historicoService.inserir(new Historico("Campeonato", "Atualiza��o #" + idCampeonato, usuario.getId()));
				return "redirect:campeonatos";
			} else {
				return "campeonato/editar-campeonato";
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "index";
	}
	
	/*
	 ==============================================
	 ===================DELETE=====================
	 ==============================================
	*/
	@RequestMapping("/campeonato/desativar")
	public String excluirCampeonato(@RequestParam("id") int id, HttpSession session) {
		Usuario usuario = (Usuario) session.getAttribute("usuario");
		try {
			campeonatoService.excluir(id);
			historicoService.inserir(new Historico("Campeonato", "Exclus�o #" + id, usuario.getId()));
			return "redirect:/campeonatos";
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "redirect:/inicio";
	}
}

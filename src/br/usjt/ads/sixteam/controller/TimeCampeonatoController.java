package br.usjt.ads.sixteam.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.usjt.ads.sixteam.model.entity.Campeonato;
import br.usjt.ads.sixteam.model.entity.Grupo;
import br.usjt.ads.sixteam.model.entity.Historico;
import br.usjt.ads.sixteam.model.entity.Time;
import br.usjt.ads.sixteam.model.entity.TimeCampeonato;
import br.usjt.ads.sixteam.model.entity.Usuario;
import br.usjt.ads.sixteam.model.service.CampeonatoService;
import br.usjt.ads.sixteam.model.service.EstatisticaService;
import br.usjt.ads.sixteam.model.service.GrupoService;
import br.usjt.ads.sixteam.model.service.HistoricoService;
import br.usjt.ads.sixteam.model.service.JogoService;
import br.usjt.ads.sixteam.model.service.TimeCampeonatoService;
import br.usjt.ads.sixteam.model.service.TimeJogadorService;
import br.usjt.ads.sixteam.model.service.TimeService;

@Controller
public class TimeCampeonatoController {
	
	@Autowired
	private CampeonatoService campeonatoService;
	@Autowired
	private HistoricoService historicoService;
	@Autowired
	private TimeCampeonatoService timeCampeonatoService;
	@Autowired
	private TimeService timeService;
	@Autowired
	private TimeJogadorService timeJogadorService;
	@Autowired
	private GrupoService grupoService;
	@Autowired
	private JogoService jogoService;
	@Autowired
	private EstatisticaService estatisticaService;
	
	/*
	 ==============================================
	 =============Formata��o de Data===============
	 ==============================================
	*/
	@InitBinder
	public void initBinder(WebDataBinder binder) {
	    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
	    dateFormat.setLenient(false);
	    binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
	}
	
	
	/*
	 ==============================================
	 ====================TELA======================
	 ==========Cadastrar Time/Campeonato===========
	 ==============================================
	*/
	@RequestMapping("/registrar_equipe")
	public String novoTimeCampeonato(@RequestParam("campeonato") int idCampeonato, HttpSession session) {
		try {
			Campeonato campeonato = campeonatoService.buscar(idCampeonato);
			List<Time> times = timeService.buscarTodosPorCategoria(campeonato.getCategoria().getId());
			List<TimeCampeonato> timesCadastrados = timeCampeonatoService.buscarTodosPorCampeonato(idCampeonato);
			List<Grupo> grupos = grupoService.buscarListaGrupos(campeonato.getRegra().getQuantidadeGrupos());
			System.out.println(times);
			if(timesCadastrados.size() > 0) {
				times = timeService.filtrar(times, timesCadastrados);
				grupos = grupoService.filtrar(grupos, timesCadastrados, campeonato.getRegra().getQuantidadeTimes());
			}
			System.out.println(times);
			session.setAttribute("campeonato", campeonato);
			session.setAttribute("times", times);
			session.setAttribute("grupos", grupos);
			
			return "equipe/nova-equipe-campeonato";
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "index";
	}
	
	/*
	 ==============================================
	 ====================TELA======================
	 ============Editar Time/Campeonato============
	 ==============================================
	*/
	@RequestMapping("/campeonato/equipe/editar/{id}")
	public String telaEditarTimeCampeonato(@PathVariable("id") int idTimeCampeonato, HttpSession session) {
		try {
			TimeCampeonato timeCampeonato = timeCampeonatoService.buscar(idTimeCampeonato);
			session.setAttribute("timeCampeonato", timeCampeonato);
			session.setAttribute("grupos", grupoService.buscarListaGrupos(timeCampeonato.getCampeonato().getRegra().getQuantidadeGrupos()));
			return "equipe/editar-equipe-campeonato";
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "redirect:/";
	}
	
	/*
	 ==============================================
	 ====================TELA======================
	 =========Visualizar Time/Campeonato===========
	 ==============================================
	*/
	@RequestMapping("/equipes/visualizar/{id}")
	public String telaVisualizarEquipe(@PathVariable("id") int idEquipe, HttpSession session) {
		try {
			TimeCampeonato timeCampeonato = timeCampeonatoService.buscar(idEquipe);
			session.setAttribute("timeCampeonato", timeCampeonato);
			session.setAttribute("titulares", timeJogadorService.buscarTodosTitularesPorEquipe(timeCampeonato.getId()));
			session.setAttribute("reservas", timeJogadorService.buscarTodosReservasPorEquipe(timeCampeonato.getId()));
			session.setAttribute("listaJogos", jogoService.buscarTodosPorEquipe(timeCampeonato));
			return "equipe/visualizar-equipe-campeonato";
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "redirect:/inicio";
	}
	
	/*
	 ==============================================
	 ====================TELA======================
	 ==========Lista Time/Campeonato===========
	 ==============================================
	*/
	@RequestMapping("/equipes/campeonato/{id}")
	public String listaEquipesPorCampeonato(@PathVariable("id") int idCampeonato, HttpSession session) throws IOException {
		try {
			session.setAttribute("campeonato", campeonatoService.buscar(idCampeonato));
			session.setAttribute("listaTimesCampeonato", timeCampeonatoService.buscarTodosPorCampeonato(idCampeonato));
			return "equipe/lista-equipes-campeonato";
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "redirect:/";
	}
	
	// ========================================== CRUD ================================================================================
	
	/*
	 ==============================================
	 ===================CREATE=====================
	 ==============================================
	*/
	@RequestMapping("/inserir_time_campeonato")
	public String inserirTimeCampeonato(TimeCampeonato timeCampeonato, HttpSession session) {
		Usuario usuario = (Usuario) session.getAttribute("usuario");
		try {
			timeCampeonatoService.inserir(timeCampeonato);
			historicoService.inserir(new Historico("Equipe/Campeonato", "Inser��o do Time #" + timeCampeonato.getTime().getId() + " no Campeonato #" + timeCampeonato.getCampeonato().getId(), usuario.getId()));
			return "redirect:/equipes/campeonato/" + timeCampeonato.getCampeonato().getId();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "redirect:/";
	}
	
	/*
	 ==============================================
	 ===================UPDATE=====================
	 ==============================================
	*/
	@RequestMapping("/atualizar_time_campeonato")
	public String atualizarTimeCampeonato(TimeCampeonato timeCampeonato, HttpSession session) {
		Usuario usuario = (Usuario) session.getAttribute("usuario");
		try {
			timeCampeonatoService.atualizar(timeCampeonato);
			historicoService.inserir(new Historico("Equipe/Campeonato", "Atualiza��o do Time #" + timeCampeonato.getTime().getId() + " no Campeonato #" + timeCampeonato.getCampeonato().getId(), usuario.getId()));
			return "redirect:/equipes/campeonato/" + timeCampeonato.getCampeonato().getId();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "redirect:/";
	}
	
	/*
	 ==============================================
	 ===================DELETE=====================
	 ==============================================
	*/
	@RequestMapping("/equipe/excluir_de_campeonato")
	public String excluirTimeDeCampeonato(@RequestParam("id") int id, HttpSession session, HttpServletRequest request) {
		Usuario usuario = (Usuario) session.getAttribute("usuario");
		String referer = request.getHeader("Referer");
		try {
			TimeCampeonato timeCampeonato = timeCampeonatoService.buscar(id);
			timeJogadorService.excluirTodosDeEquipe(timeCampeonatoService.buscar(id).getTime());
			timeCampeonatoService.excluir(id);
			historicoService.inserir(new Historico("Equipe/Campeonato", "Exclus�o do Time #" + timeCampeonato.getTime().getId() + " do Campeonato #" + timeCampeonato.getCampeonato().getId(), usuario.getId()));
			return "redirect:" + referer;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "redirect:/";
	}
	
}

package br.usjt.ads.sixteam.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.usjt.ads.sixteam.model.entity.Historico;
import br.usjt.ads.sixteam.model.entity.Tecnico;
import br.usjt.ads.sixteam.model.entity.Usuario;
import br.usjt.ads.sixteam.model.service.HistoricoService;
import br.usjt.ads.sixteam.model.service.TecnicoService;

@Controller
public class TecnicoController {
	
	@Autowired
	private HistoricoService historicoService;
	@Autowired
	private TecnicoService tecnicoService;
	
	/*
	 ==============================================
	 =============Formata��o de Data===============
	 ==============================================
	*/
	@InitBinder
	public void initBinder(WebDataBinder binder) {
	    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
	    dateFormat.setLenient(false);
	    binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
	}
	
	/*
	 ==============================================
	 ====================TELA======================
	 ==============Editar Treinador================
	 ==============================================
	*/
	@RequestMapping("/treinadores/editar/{id}")
	public String telaEditarTreinador(@PathVariable("id") int id, HttpSession session) {
		try {
			Tecnico tecnico = tecnicoService.buscar(id);
			session.setAttribute("treinador", tecnico);
			return "tecnico/editar-tecnico";
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "redirect:/treinadores";
	}
	
	/*
	 ==============================================
	 ====================TELA======================
	 =============Cadastrar Treinador==============
	 ==============================================
	*/
	@RequestMapping("/treinador/cadastro")
	public String novoTreinador() {
		return "tecnico/novo-tecnico";
	}
	
	/*
	 ==============================================
	 ====================TELA======================
	 =============Lista de Treinadores=============
	 ==============================================
	*/
	@RequestMapping("/treinadores")
	public String treinadores(HttpSession session) {
		List<Tecnico> listaTreinadores;
		try {
			listaTreinadores = tecnicoService.buscarTodos();
			session.setAttribute("listaTreinadores", listaTreinadores);
			return "/tecnico/lista-tecnicos";
		} catch (IOException e) {
			session.setAttribute("listaTreinadores", null);
			e.printStackTrace();
		}
		return "redirect:/inicio";
	}
	
	// ========================================== CRUD ================================================================================
	
	/*
	 ==============================================
	 ===================CREATE=====================
	 ==============================================
	*/
	@RequestMapping("/inserir_treinador")
	public String inserirTreinador(@Valid Tecnico tecnico, BindingResult result, HttpSession session) {
		Usuario usuario = (Usuario) session.getAttribute("usuario");
		try {
			if(!result.hasFieldErrors()) {
				int idTecnico = tecnicoService.inserir(tecnico);
				historicoService.inserir(new Historico("Treinador", "Inclus�o #" + idTecnico, usuario.getId()));
				return "redirect:/treinadores";
			} else {
				return "tecnico/novo-tecnico";
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "index";
	}
	
	/*
	 ==============================================
	 ===================UPDATE=====================
	 ==============================================
	*/
	@RequestMapping("/atualizar_treinador")
	public String atualizarTreinador(@Valid Tecnico tecnico, BindingResult result, HttpSession session) {
		Usuario usuario = (Usuario) session.getAttribute("usuario");
		try {
			if(!result.hasFieldErrors()) {
				tecnicoService.atualizar(tecnico);
				historicoService.inserir(new Historico("Treinador", "Atualiza��o #" + tecnico.getId(), usuario.getId()));
				return "redirect:/treinadores";
			} else {
				return "tecnico/editar-tecnico";
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "redirect:/inicio";
	}
	
	/*
	 ==============================================
	 ===================DELETE=====================
	 ==============================================
	*/
	@RequestMapping("/treinador/desativar")
	public String excluirTecnico(@RequestParam("id") int id, HttpSession session) {
		Usuario usuario = (Usuario) session.getAttribute("usuario");
		try {
			tecnicoService.excluir(id);
			historicoService.inserir(new Historico("Treinador", "Exclus�o #" + id, usuario.getId()));
			return "redirect:/treinadores";
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "redirect:/inicio";
	}
}

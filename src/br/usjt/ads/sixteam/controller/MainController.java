package br.usjt.ads.sixteam.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;

import br.usjt.ads.sixteam.model.entity.Usuario;
import br.usjt.ads.sixteam.model.service.LoginService;

@Controller
public class MainController {
	
	@Autowired
	private LoginService loginService;
	
	/*
	 ==============================================
	 =============Formatação de Data===============
	 ==============================================
	*/
	@InitBinder
	public void initBinder(WebDataBinder binder) {
	    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
	    dateFormat.setLenient(false);
	    binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
	}
	
	/*
	 ==============================================
	 ===============Requests Padrões===============
	 ==============================================
	*/
	
	// Carrega Root
	@RequestMapping("/")
	public String start() {
		return "index";
	}
	
	// Redireciona para Root
	@RequestMapping("/inicio")
	public String inicio() {
		return "redirect:/";
	}
	
	/*
	 ==============================================
	 ================Requests Login================
	 ==============================================
	*/
	
	// Faz Login
	@RequestMapping("/fazer_login")
	public String fazerLogin(HttpSession session, Usuario usuario) {
		try {
			usuario = loginService.login(usuario);
			session.setAttribute("usuario", usuario);
			if(usuario != null) {
				return "redirect:administracao";
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "redirect:inicio";
	}
	
	// Faz Logout
	@RequestMapping("/fazer_logout")
	public String deslogar(HttpSession session) {
		session.removeAttribute("usuario");
		return "redirect:inicio";
	}
	
	//Carrega Tela de Administração
	@RequestMapping("/administracao")
	public String administracao() {
		return "administracao";
	}	
}

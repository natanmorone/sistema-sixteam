package br.usjt.ads.sixteam.controller;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.usjt.ads.sixteam.model.entity.Campeonato;
import br.usjt.ads.sixteam.model.entity.Estatistica;
import br.usjt.ads.sixteam.model.entity.Historico;
import br.usjt.ads.sixteam.model.entity.Jogo;
import br.usjt.ads.sixteam.model.entity.TimeCampeonato;
import br.usjt.ads.sixteam.model.entity.TimeJogador;
import br.usjt.ads.sixteam.model.entity.Usuario;
import br.usjt.ads.sixteam.model.service.AcaoService;
import br.usjt.ads.sixteam.model.service.ArbitroService;
import br.usjt.ads.sixteam.model.service.CampeonatoService;
import br.usjt.ads.sixteam.model.service.EstatisticaService;
import br.usjt.ads.sixteam.model.service.HistoricoService;
import br.usjt.ads.sixteam.model.service.JogoService;
import br.usjt.ads.sixteam.model.service.LocalService;
import br.usjt.ads.sixteam.model.service.TimeCampeonatoService;
import br.usjt.ads.sixteam.model.service.TimeJogadorService;
import br.usjt.ads.sixteam.model.service.TimeService;

@Controller
public class JogoController {
	
	@Autowired
	private CampeonatoService campeonatoService;
	@Autowired
	private TimeService timeService;
	@Autowired
	private TimeJogadorService timeJogadorService;
	@Autowired
	private HistoricoService historicoService;
	@Autowired
	private ArbitroService arbitroService;
	@Autowired
	private LocalService localService;
	@Autowired
	private JogoService jogoService;
	@Autowired
	private TimeCampeonatoService timeCampeonatoService;
	@Autowired
	private AcaoService acaoService;
	@Autowired
	private EstatisticaService estatisticaService;
	
	/*
	 ==============================================
	 =============Formata��o de Data===============
	 ==============================================
	*/
	@InitBinder
	public void initBinder(WebDataBinder binder) {
	    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
	    dateFormat.setLenient(false);
	    binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
	}
	
	/*
	 ==============================================
	 ====================TELA======================
	 ==============Cadastrar Jogo==================
	 ==============================================
	*/
	@RequestMapping("/registrar_jogo")
	public String novoJogoCampeonato(@RequestParam("campeonato") int idCampeonato, HttpSession session) {
		try {
			Campeonato campeonato = campeonatoService.buscar(idCampeonato);
			session.setAttribute("campeonato", campeonato);
			session.setAttribute("arbitros", arbitroService.buscarTodos());
			session.setAttribute("locais", localService.buscarTodos());
			session.setAttribute("times", timeCampeonatoService.buscarTodosPorCampeonato(idCampeonato));
			return "jogo/novo-jogo-campeonato";
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "redirect:/jogos/campeonato/" + idCampeonato;
	}
	
	/*
	 ==============================================
	 ====================TELA======================
	 =============Editar Jogo/Campeonato==============
	 ==============================================
	*/
	@RequestMapping("/campeonato/jogo/editar/{id}")
	public String telaEditarJogo(@PathVariable("id") int idJogo, HttpSession session) {
		try {
			Jogo jogo = jogoService.buscar(idJogo);
			session.setAttribute("jogo", jogoService.buscar(idJogo));
			session.setAttribute("campeonato", jogo.getCampeonato());
			session.setAttribute("arbitros", arbitroService.buscarTodos());
			session.setAttribute("locais", localService.buscarTodos());
			session.setAttribute("times", timeService.buscarTodos());
			return "jogo/editar-jogo-campeonato";
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "redirect:/";
	}
	
	/*
	 ==============================================
	 ====================TELA======================
	 ==============Visualizar Jogo=================
	 ==============================================
	*/
	@RequestMapping("/jogos/visualizar/{id}")
	public String telaVisualizarJogo(@PathVariable("id") int idJogo, HttpSession session) {
		try {
			Jogo jogo = jogoService.buscar(idJogo);
			session.setAttribute("jogo", jogo);
			session.setAttribute("gols", estatisticaService.buscarGols(jogo));
			session.setAttribute("faltas", estatisticaService.buscarFaltas(jogo));
			session.setAttribute("cartoesAmarelos", estatisticaService.buscarCA(jogo));
			session.setAttribute("cartoesVermelhos", estatisticaService.buscarCV(jogo));
			session.setAttribute("substituicoes", estatisticaService.buscarSubstituicoes(jogo));
			System.out.println("GOLS CASA >>>>>>>>>>>>>>>>>>" + estatisticaService.buscarGols(jogo).stream().filter(c -> Integer.valueOf(1).equals(c.getTime().getId())).count());
			System.out.println("GOLS VISITANTE >>>>>>>>>>>>>>>>>>" + estatisticaService.buscarGols(jogo).stream().filter(c -> Integer.valueOf(2).equals(c.getTime().getId())).count());
			
			return "jogo/visualizar-jogo-campeonato";
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "redirect:/inicio";
	}
	
	/*
	 ==============================================
	 ====================TELA======================
	 ===========Lista Jogos/Campeonato=============
	 ==============================================
	*/
	@RequestMapping("/jogos/campeonato/{id}")
	public String listaJogosCampeonato(@PathVariable("id") int idCampeonato, HttpSession session) throws IOException {
		try {
			session.setAttribute("campeonato", campeonatoService.buscar(idCampeonato));
			session.setAttribute("listaJogosCampeonato", jogoService.buscarTodosPorCampeonato(idCampeonato));
			return "jogo/lista-jogos-campeonato";
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "redirect:/";
	}
	
	/*
	 ==============================================
	 ====================TELA======================
	 ===============Inserir Evento=================
	 ==============================================
	*/
	@RequestMapping("/campeonato/jogo/resultado/{id}")
	public String telaInserirResultado(@PathVariable("id") int idJogo, HttpSession session) throws IOException {
		Jogo jogo = jogoService.buscar(idJogo);
		session.setAttribute("jogo", jogo);
		session.setAttribute("acoes", acaoService.buscarTodas());
		List<TimeCampeonato> listaDeTimes = new ArrayList<TimeCampeonato>();
		listaDeTimes.add(timeCampeonatoService.buscar(jogo.getTimeCasa().getId()));
		listaDeTimes.add(timeCampeonatoService.buscar(jogo.getTimeVisitante().getId()));
		session.setAttribute("times", listaDeTimes);
		List<TimeJogador> listaJogadores = new ArrayList<TimeJogador>();
		listaJogadores.addAll(timeJogadorService.buscarTodosPorEquipe(jogo.getTimeCasa().getId()));
		listaJogadores.addAll(timeJogadorService.buscarTodosPorEquipe(jogo.getTimeVisitante().getId()));
		session.setAttribute("jogadores", listaJogadores);
		return "jogo/inserir-resultado";
	}
	
	// ========================================== CRUD ================================================================================
	
	/*
	 ==============================================
	 ===================CREATE=====================
	 ==============================================
	*/
	@RequestMapping("/inserir_jogo_campeonato")
	public String inserirJogoCampeonato(Jogo jogo, @RequestParam("horaDaPartida") String horaDaPartida, HttpSession session) {
		Usuario usuario = (Usuario) session.getAttribute("usuario");
		SimpleDateFormat novaHora = new SimpleDateFormat("hh:mm");
		long ms;
		try {
			ms = novaHora.parse(horaDaPartida).getTime();
			java.sql.Time hora = new java.sql.Time(ms);
			jogo.setHora(hora);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		int idJogo;
		try {
			idJogo = jogoService.inserir(jogo);
			historicoService.inserir(new Historico("Jogo", "Inclus�o do Jogo #" + idJogo + " no Campeonato #" + jogo.getCampeonato().getId(), usuario.getId()));
			return "redirect:/jogos/campeonato/" + jogo.getCampeonato().getId();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "redirect:/";
	}
	
	/*
	 ==============================================
	 ===================DELETE=====================
	 ==============================================
	*/
	@RequestMapping("/jogo/excluir_de_campeonato")
	public String excluirJogadorDeEquipe(@RequestParam("id") int id, HttpSession session, HttpServletRequest request) {
		Usuario usuario = (Usuario) session.getAttribute("usuario");
		String referer = request.getHeader("Referer");
		try {
			jogoService.excluir(id);
			historicoService.inserir(new Historico("Jogo", "Exclus�o do Jogo #" + id, usuario.getId()));
			return "redirect:" + referer;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "redirect:/";
	}
	
	@RequestMapping(value="/jogo/inserir_resultado")
	public String inserirResultado(Estatistica estatistica, HttpSession session) {
		Usuario usuario = (Usuario) session.getAttribute("usuario");
		try {
			int idEstatistica = estatisticaService.inserir(estatistica);
			historicoService.inserir(new Historico("Estatistica", "Inclus�o de Estat�stica #" + idEstatistica + " no Jogo #" + estatistica.getJogo().getId(), usuario.getId()));
			return "redirect:/jogos/campeonato/" + estatisticaService.buscar(idEstatistica).getJogo().getCampeonato().getId();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "redirect:/";
	}
}

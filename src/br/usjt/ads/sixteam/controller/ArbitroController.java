package br.usjt.ads.sixteam.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.usjt.ads.sixteam.model.entity.Arbitro;
import br.usjt.ads.sixteam.model.entity.Historico;
import br.usjt.ads.sixteam.model.entity.Usuario;
import br.usjt.ads.sixteam.model.service.ArbitroService;
import br.usjt.ads.sixteam.model.service.HistoricoService;

@Controller
public class ArbitroController {
	
	@Autowired
	private HistoricoService historicoService;
	@Autowired
	private ArbitroService arbitroService;
	
	/*
	 ==============================================
	 =============Formata��o de Data===============
	 ==============================================
	*/
	@InitBinder
	public void initBinder(WebDataBinder binder) {
	    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
	    dateFormat.setLenient(false);
	    binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
	}
	
	/*
	 ==============================================
	 ====================TELA======================
	 ================Editar �rbitro================
	 ==============================================
	*/
	@RequestMapping("/arbitros/editar/{id}")
	public String telaEditarArbitro(@PathVariable("id") int id, HttpSession session) {
		try {
			Arbitro arbitro = arbitroService.buscar(id);
			session.setAttribute("arbitro", arbitro);
			return "arbitro/editar-arbitro";
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "redirect:/arbitros";
	}
	
	/*
	 ==============================================
	 ====================TELA======================
	 =============Cadastrar �rbitro================
	 ==============================================
	*/
	@RequestMapping("/arbitro/cadastro")
	public String novoArbitro() {
		return "arbitro/novo-arbitro";
	}
	
	/*
	 ==============================================
	 ====================TELA======================
	 ==============Lista de �rbitros===============
	 ==============================================
	*/
	@RequestMapping("/arbitros")
	public String arbitros(HttpSession session) {
		List<Arbitro> listaArbitros;
		try {
			listaArbitros = arbitroService.buscarTodos();
			session.setAttribute("listaArbitros", listaArbitros);
			return "/arbitro/lista-arbitros";
		} catch (IOException e) {
			session.setAttribute("listaArbitros", null);
			e.printStackTrace();
		}
		return "redirect:/inicio";
	}
	
	// ========================================== CRUD ================================================================================
	
	/*
	 ==============================================
	 ===================CREATE=====================
	 ==============================================
	*/
	@RequestMapping("/inserir_arbitro")
	public String inserirArbitro(@Valid Arbitro arbitro, BindingResult result, HttpSession session) {
		Usuario usuario = (Usuario) session.getAttribute("usuario");
		try {
			if(!result.hasFieldErrors()) {
				int idArbitro = arbitroService.inserir(arbitro);
				historicoService.inserir(new Historico("�rbitro", "Inclus�o #" + idArbitro, usuario.getId()));
				return "redirect:/arbitros";
			} else {
				return "arbitro/novo-arbitro";
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "index";
	}
	
	/*
	 ==============================================
	 ===================UPDATE=====================
	 ==============================================
	*/
	@RequestMapping("/atualizar_arbitro")
	public String atualizarArbitro(@Valid Arbitro arbitro, BindingResult result, HttpSession session) {
		Usuario usuario = (Usuario) session.getAttribute("usuario");
		try {
			if(!result.hasFieldErrors()) {
				arbitroService.atualizar(arbitro);
				historicoService.inserir(new Historico("�rbitro", "Atualiza��o #" + arbitro.getId(), usuario.getId()));
				return "redirect:/arbitros";
			} else {
				return "arbitro/editar-arbitro";
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "redirect:/inicio";
	}
	
	/*
	 ==============================================
	 ===================DELETE=====================
	 ==============================================
	*/
	@RequestMapping("/arbitro/desativar")
	public String excluirTecnico(@RequestParam("id") int id, HttpSession session) {
		Usuario usuario = (Usuario) session.getAttribute("usuario");
		try {
			arbitroService.excluir(id);
			historicoService.inserir(new Historico("�rbitro", "Exclus�o #" + id, usuario.getId()));
			return "redirect:/arbitros";
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "redirect:/inicio";
	}
}

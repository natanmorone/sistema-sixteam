package br.usjt.ads.sixteam.model.service;

import java.io.IOException;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.usjt.ads.sixteam.model.dao.JogoDAO;
import br.usjt.ads.sixteam.model.entity.Jogo;
import br.usjt.ads.sixteam.model.entity.TimeCampeonato;

@Service
public class JogoService {
	@Autowired
	private JogoDAO jogoDAO;
	
	@Transactional
	public int inserir(Jogo jogo) throws IOException {
		return jogoDAO.inserir(jogo);
	}
	
	@Transactional
	public void atualizar(Jogo jogo) throws IOException {
		jogoDAO.atualizar(jogo);
	}
	
	@Transactional
	public void excluir(int id) throws IOException {
		jogoDAO.excluir(id);
	}
	
	@Transactional
	public Jogo buscar(int id) throws IOException {
		return jogoDAO.buscar(id);
	}
	
	@Transactional
	public List<Jogo> buscarTodos() throws IOException {
		return jogoDAO.buscarTodos();
	}
	
	@Transactional
	public List<Jogo> buscarTodosPorCampeonato(int idCampeonato) throws IOException {
		return jogoDAO.buscarTodosPorCampeonato(idCampeonato);
	}
	
	@Transactional
	public List<Jogo> buscarTodosPorEquipe(TimeCampeonato tc) throws IOException {
		return jogoDAO.buscarTodosPorEquipe(tc);
	}
	
}

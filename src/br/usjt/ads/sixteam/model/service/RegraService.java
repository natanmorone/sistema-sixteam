package br.usjt.ads.sixteam.model.service;

import java.io.IOException;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.usjt.ads.sixteam.model.dao.RegraDAO;
import br.usjt.ads.sixteam.model.entity.Regra;

@Service
public class RegraService {
	
	@Autowired
	private RegraDAO regraDAO;
	
	@Transactional
	public int inserir(Regra regra) throws IOException {
		return regraDAO.inserir(regra);
	}
	
	@Transactional
	public void atualizar(Regra regra) throws IOException {
		regraDAO.atualizar(regra);
	}

}

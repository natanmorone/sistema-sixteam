package br.usjt.ads.sixteam.model.service;

import java.io.IOException;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.usjt.ads.sixteam.model.dao.JogadorDAO;
import br.usjt.ads.sixteam.model.entity.Jogador;

@Service
public class JogadorService {
	@Autowired
	private JogadorDAO jogadorDAO;
	
	@Transactional
	public int inserir(Jogador jogador) throws IOException {
		return jogadorDAO.inserir(jogador);
	}
	
	@Transactional
	public void atualizar(Jogador jogador) throws IOException {
		jogadorDAO.atualizar(jogador);
	}
	
	@Transactional
	public void excluir(int id) throws IOException {
		jogadorDAO.excluir(id);
	}
	
	@Transactional
	public Jogador buscar(int id) throws IOException {
		return jogadorDAO.buscar(id);
	}
	
	@Transactional
	public List<Jogador> buscarTodos() throws IOException {
		return jogadorDAO.buscarTodos();
	}
	
	@Transactional
	public List<Jogador> buscarPorCategoria(int id) throws IOException {
		return jogadorDAO.buscarPorCategoria(id);
	}
	
	@Transactional
	public void ocupar(int id) throws IOException {
		jogadorDAO.ocupar(id);
	}
	
	@Transactional
	public void desocupar(int id) throws IOException {
		jogadorDAO.desocupar(id);
	}
	
}

package br.usjt.ads.sixteam.model.service;

import java.io.IOException;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.usjt.ads.sixteam.model.dao.CampeonatoDAO;
import br.usjt.ads.sixteam.model.entity.Campeonato;

@Service
public class CampeonatoService {
	@Autowired
	private CampeonatoDAO campeonatoDAO;
	
	@Transactional
	public int inserir(Campeonato campeonato) throws IOException {
		return campeonatoDAO.inserir(campeonato);
	}
	
	@Transactional
	public void atualizar(Campeonato campeonato) throws IOException {
		campeonatoDAO.atualizar(campeonato);
	}
	
	@Transactional
	public void excluir(int id) throws IOException {
		campeonatoDAO.excluir(id);
	}
	
	public List<Campeonato> buscarTodos() throws IOException {
		return campeonatoDAO.buscarTodos();
	}
	
	@Transactional
	public Campeonato buscar(int id) throws IOException {
		return campeonatoDAO.buscar(id);
	}
	
	@Transactional
	public Campeonato buscarPorTime(int id) throws IOException {
		return campeonatoDAO.buscarPorTime(id);
	}
	
}

package br.usjt.ads.sixteam.model.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import br.usjt.ads.sixteam.model.dao.TimeDAO;
import br.usjt.ads.sixteam.model.entity.Time;
import br.usjt.ads.sixteam.model.entity.TimeCampeonato;

@Service
public class TimeService {
	@Autowired
	private TimeDAO timeDAO;
	
	@Transactional
	public int inserir(Time time) throws IOException {
		return timeDAO.inserir(time);
	}
	
	@Transactional
	public void atualizar(Time time) throws IOException {
		timeDAO.atualizar(time);
	}
	
	@Transactional
	public void excluir(int id) throws IOException {
		timeDAO.excluir(id);
	}
	
	@Transactional
	public Time buscar(int id) throws IOException {
		return timeDAO.buscar(id);
	}
	
	@Transactional
	public boolean isTimeCadastrado(String nome) throws IOException {
		return timeDAO.isTimeCadastrado(nome);
	}
	
	@Transactional
	public List<Time> buscarTodosPorCategoria(int id) throws IOException {
		return timeDAO.buscarTodosPorCategoria(id);
	}
	
	@Transactional
	public List<Time> buscarTodos() throws IOException {
		return timeDAO.buscarTodos();
	}
	
	public List<Time> filtrar(List<Time> times, List<TimeCampeonato> timesCadastrados) {
		List<Time> novaLista = new ArrayList<Time>(times);
		for (int i = 0; i < times.size(); i++) {
			for (TimeCampeonato timeCadastrado : timesCadastrados) {
				if (timeCadastrado.getTime().getId() == times.get(i).getId()) {
					novaLista.remove(times.get(i));
				}
			}
		}
		return novaLista;
	}
	
}

package br.usjt.ads.sixteam.model.service;

import java.io.IOException;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.usjt.ads.sixteam.model.dao.CategoriaDAO;
import br.usjt.ads.sixteam.model.entity.Categoria;

@Service
public class CategoriaService {
	@Autowired
	private CategoriaDAO categoriaDAO;
	
	public List<Categoria> buscarTodas() throws IOException {
		return categoriaDAO.buscarTodas();
	}
	
	@Transactional
	public Categoria buscar(int id) throws IOException {
		return categoriaDAO.buscar(id);
	}
	
}

package br.usjt.ads.sixteam.model.service;

import java.io.IOException;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.usjt.ads.sixteam.model.dao.ArbitroDAO;
import br.usjt.ads.sixteam.model.entity.Arbitro;


@Service
public class ArbitroService {
	@Autowired
	private ArbitroDAO arbitroDAO;
	
	@Transactional
	public int inserir(Arbitro arbitro) throws IOException {
		return arbitroDAO.inserir(arbitro);
	}
	
	@Transactional
	public void atualizar(Arbitro arbitro) throws IOException {
		arbitroDAO.atualizar(arbitro);
	}
	
	@Transactional 
	public Arbitro buscar(int id) throws IOException {
		return arbitroDAO.buscar(id);
	}
	
	@Transactional
	public void excluir(int id) throws IOException {
		arbitroDAO.excluir(id);
	}
	
	@Transactional
	public List<Arbitro> buscarTodos() throws IOException {
		return arbitroDAO.buscarTodos();
	}
}

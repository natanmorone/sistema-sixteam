package br.usjt.ads.sixteam.model.dao;

import java.io.IOException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import br.usjt.ads.sixteam.model.entity.Acao;


@Repository
public class AcaoDAO {
	@PersistenceContext
	EntityManager manager;
	
	public Acao buscar(int id) throws IOException {
		return manager.find(Acao.class, id);
	}
	
	@SuppressWarnings("unchecked")
	public List<Acao> buscarTodas() throws IOException {
		return manager.createQuery("select a from Acao a order by a.id asc").getResultList();
	}
	
}

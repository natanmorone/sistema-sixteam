package br.usjt.ads.sixteam.model.dao;

import java.io.IOException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import br.usjt.ads.sixteam.model.entity.Campeonato;

@Repository
public class CampeonatoDAO {
	@PersistenceContext
	EntityManager manager;
	
	public int inserir(Campeonato campeonato) throws IOException {
		manager.persist(campeonato);
		return campeonato.getId();
	}
	
	public void atualizar(Campeonato campeonato) throws IOException {
		manager.merge(campeonato);
	}
	
	public void excluir(int id) throws IOException {
		Query query = manager.createQuery("update Campeonato c set c.ativo = false where c.id = :id");
		query.setParameter("id", id);
		query.executeUpdate();
	}
	
	public Campeonato buscar(int id) {
		return manager.find(Campeonato.class, id);
	}
	
	@SuppressWarnings("unchecked")
	public List<Campeonato> buscarTodos() throws IOException {
		return manager.createQuery("select c from Campeonato c where c.ativo = true order by c.id asc").getResultList();
	}
	
	public Campeonato buscarPorTime(int id) throws IOException {
		Query query = manager.createQuery("select tc.campeonato from TimeCampeonato tc where tc.time.id = :id");
		query.setParameter("id", id);
		return (Campeonato) query.getSingleResult();
	}
}
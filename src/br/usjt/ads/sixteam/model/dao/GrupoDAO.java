package br.usjt.ads.sixteam.model.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import br.usjt.ads.sixteam.model.entity.Grupo;

@Repository
public class GrupoDAO {
	@PersistenceContext
	EntityManager manager;

	@SuppressWarnings("unchecked")
	public List<Grupo> buscarListaGrupos(int quantidade) {
		Query query = manager.createQuery("select g from Grupo g").setMaxResults(quantidade);
		return query.getResultList();
	}

}

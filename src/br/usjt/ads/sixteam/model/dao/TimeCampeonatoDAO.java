package br.usjt.ads.sixteam.model.dao;

import java.io.IOException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import br.usjt.ads.sixteam.model.entity.TimeCampeonato;

@Repository
public class TimeCampeonatoDAO {
	@PersistenceContext
	EntityManager manager;
	
	public void inserir(TimeCampeonato timeCampeonato) throws IOException {
		manager.persist(timeCampeonato);
	}
	
	public void excluir(int id) throws IOException {
		manager.remove(manager.find(TimeCampeonato.class, id));
	}
	
	public void atualizar(TimeCampeonato timeCampeonato) throws IOException {
		manager.merge(timeCampeonato);
	}
	
	public TimeCampeonato buscar(int id) throws IOException {
		return manager.find(TimeCampeonato.class, id);
	}
	
	@SuppressWarnings("unchecked")
	public List<TimeCampeonato> buscarTodos() throws IOException {
		return manager.createQuery("select tc from TimeCampeonato tc where tc.time.ativo = true order by tc.id desc").getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<TimeCampeonato> buscarTodosPorCampeonato(int idCampeonato) throws IOException {
		Query query = manager.createQuery("select tc from TimeCampeonato tc where tc.time.ativo = true and tc.campeonato.id = :id");
		query.setParameter("id", idCampeonato);
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public TimeCampeonato buscarPorTimeECampeonato(int idTime, int idCampeonato) {
		Query query = manager.createQuery("select tc from TimeCampeonato tc where tc.time.id = :idT and tc.campeonato.id = :idC");
		query.setParameter("idT", idTime);
		query.setParameter("idC", idCampeonato);
		List<TimeCampeonato> resultado = query.getResultList();
		if (resultado.size() > 0) {
			return (TimeCampeonato) query.getSingleResult();
		} else {
			return null;
		}
	}
	
}
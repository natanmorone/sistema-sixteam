package br.usjt.ads.sixteam.model.dao;

import java.io.IOException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import br.usjt.ads.sixteam.model.entity.Regra;

@Repository
public class RegraDAO {
	@PersistenceContext
	EntityManager manager;
	
	public int inserir(Regra regra) throws IOException {
		manager.persist(regra);
		return regra.getId();
	}
	
	public void atualizar(Regra regra) throws IOException {
		manager.merge(regra);
	}
	
}

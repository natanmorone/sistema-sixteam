package br.usjt.ads.sixteam.model.dao;

import java.io.IOException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import br.usjt.ads.sixteam.model.entity.Historico;

@Repository
public class HistoricoDAO {
	@PersistenceContext
	EntityManager manager;
	
	@Transactional
	public void inserir(Historico historico) throws IOException {
		manager.persist(historico);
	}
}

package br.usjt.ads.sixteam.model.dao;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import br.usjt.ads.sixteam.model.entity.Estatistica;
import br.usjt.ads.sixteam.model.entity.Jogo;
import br.usjt.ads.sixteam.model.entity.TimeCampeonato;
import br.usjt.ads.sixteam.model.entity.TimeJogador;

@Repository
public class EstatisticaDAO {
	@PersistenceContext
	EntityManager manager;
	
	public int inserir(Estatistica estatistica) {
		manager.persist(estatistica);
		return estatistica.getId();
	}
	
	public Estatistica buscar(int id) {
		return manager.find(Estatistica.class, id);
	}
	
	
	@SuppressWarnings("unchecked")
	public List<Estatistica> buscarGols(Jogo jogo) throws IOException {
		List<Estatistica> gols = new ArrayList<Estatistica>();
		gols.addAll(manager.createQuery("select e from Estatistica e where e.jogo.id = :idJogo and e.acao.id = 1").setParameter("idJogo", jogo.getId()).getResultList());
		gols.addAll(manager.createQuery("select e from Estatistica e where e.jogo.id = :idJogo and e.acao.id = 2").setParameter("idJogo", jogo.getId()).getResultList());
		gols.addAll(manager.createQuery("select e from Estatistica e where e.jogo.id = :idJogo and e.acao.id = 3").setParameter("idJogo", jogo.getId()).getResultList());
		gols.addAll(manager.createQuery("select e from Estatistica e where e.jogo.id = :idJogo and e.acao.id = 4").setParameter("idJogo", jogo.getId()).getResultList());
		gols.sort(Comparator.comparing(Estatistica::getTempo));
		return gols;
	}
	
	@SuppressWarnings("unchecked")
	public List<Estatistica> buscarGols(TimeJogador jogador) throws IOException {
		List<Estatistica> gols = new ArrayList<Estatistica>();
		gols.addAll(manager.createQuery("select e from Estatistica e where e.jogador.id = :idJogador and e.acao.id = 1").setParameter("idJogador", jogador.getId()).getResultList());
		gols.addAll(manager.createQuery("select e from Estatistica e where e.jogador.id = :idJogador and e.acao.id = 2").setParameter("idJogador", jogador.getId()).getResultList());
		gols.addAll(manager.createQuery("select e from Estatistica e where e.jogador.id = :idJogador and e.acao.id = 3").setParameter("idJogador", jogador.getId()).getResultList());
		gols.addAll(manager.createQuery("select e from Estatistica e where e.jogador.id = :idJogador and e.acao.id = 4").setParameter("idJogador", jogador.getId()).getResultList());
		gols.sort(Comparator.comparing(Estatistica::getTempo));
		return gols;
	}
	
	@SuppressWarnings("unchecked")
	public int buscarGols(TimeCampeonato time, Jogo jogo) throws IOException {
		List<Estatistica> gols = new ArrayList<Estatistica>();
		gols.addAll(manager.createQuery("select e from Estatistica e where e.time.id = :idTime and e.acao.id = 1 and e.jogo.id = :idJogo").setParameter("idTime", time.getId()).setParameter("idJogo", jogo.getId()).getResultList());
		gols.addAll(manager.createQuery("select e from Estatistica e where e.time.id = :idTime and e.acao.id = 2 and e.jogo.id = :idJogo").setParameter("idTime", time.getId()).setParameter("idJogo", jogo.getId()).getResultList());
		gols.addAll(manager.createQuery("select e from Estatistica e where e.time.id = :idTime and e.acao.id = 3 and e.jogo.id = :idJogo").setParameter("idTime", time.getId()).setParameter("idJogo", jogo.getId()).getResultList());
		gols.addAll(manager.createQuery("select e from Estatistica e where e.time.id = :idTime and e.acao.id = 4 and e.jogo.id = :idJogo").setParameter("idTime", time.getId()).setParameter("idJogo", jogo.getId()).getResultList());
		gols.sort(Comparator.comparing(Estatistica::getTempo));
		return gols.size();
	}
	
	@SuppressWarnings("unchecked")
	public List<Estatistica> buscarFaltas(Jogo jogo) throws IOException {
		Query query = manager.createQuery("select e from Estatistica e where e.jogo.id = :idJogo and e.acao.id = 5");
		query.setParameter("idJogo", jogo.getId());
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Estatistica> buscarCA(Jogo jogo) throws IOException {
		Query query = manager.createQuery("select e from Estatistica e where e.jogo.id = :idJogo and e.acao.id = 6");
		query.setParameter("idJogo", jogo.getId());
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Estatistica> buscarCA(TimeJogador jogador) throws IOException {
		Query query = manager.createQuery("select e from Estatistica e where e.jogador.id = :idJogador and e.acao.id = 6");
		query.setParameter("idJogador", jogador.getId());
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Estatistica> buscarCA(TimeCampeonato time) throws IOException {
		Query query = manager.createQuery("select e from Estatistica e where e.time.id = :idTime and e.acao.id = 6");
		query.setParameter("idTime", time.getId());
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Estatistica> buscarCV(Jogo jogo) throws IOException {
		Query query = manager.createQuery("select e from Estatistica e where e.jogo.id = :idJogo and e.acao.id = 7");
		query.setParameter("idJogo", jogo.getId());
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Estatistica> buscarCV(TimeJogador jogador) throws IOException {
		Query query = manager.createQuery("select e from Estatistica e where e.jogador.id = :idJogador and e.acao.id = 7");
		query.setParameter("idJogador", jogador.getId());
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Estatistica> buscarCV(TimeCampeonato time) throws IOException {
		Query query = manager.createQuery("select e from Estatistica e where e.time.id = :idTime and e.acao.id = 7");
		query.setParameter("idTime", time.getId());
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Estatistica> buscarSubstituicoes(Jogo jogo) throws IOException {
		Query query = manager.createQuery("select e from Estatistica e where e.jogo.id = :idJogo and e.acao.id = 9");
		query.setParameter("idJogo", jogo.getId());
		return query.getResultList();
	}
}

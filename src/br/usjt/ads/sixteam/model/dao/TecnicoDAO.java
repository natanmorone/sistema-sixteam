package br.usjt.ads.sixteam.model.dao;

import java.io.IOException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import br.usjt.ads.sixteam.model.entity.Tecnico;


@Repository
public class TecnicoDAO {
	@PersistenceContext
	EntityManager manager;
	
	public int inserir(Tecnico tecnico) throws IOException {
		manager.persist(tecnico);
		return tecnico.getId();
	}
	
	public void atualizar(Tecnico tecnico) throws IOException {
		manager.merge(tecnico);
	}
	
	public Tecnico buscar(int id) throws IOException {
		return manager.find(Tecnico.class, id);
	}
	
	public void excluir(int id) throws IOException {
		Query query = manager.createQuery("update Tecnico t set t.ativo = false where t.id = :id");
		query.setParameter("id", id);
		query.executeUpdate();
	}
	
	@SuppressWarnings("unchecked")
	public List<Tecnico> buscarTodos() throws IOException {
		return manager.createQuery("select t from Tecnico t where t.ativo = true order by t.id asc").getResultList();
	}
}

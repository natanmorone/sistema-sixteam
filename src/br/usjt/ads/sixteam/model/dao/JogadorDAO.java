package br.usjt.ads.sixteam.model.dao;

import java.io.IOException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import br.usjt.ads.sixteam.model.entity.Jogador;

@Repository
public class JogadorDAO {
	@PersistenceContext
	EntityManager manager;
	
	public int inserir(Jogador jogador) throws IOException {
		manager.persist(jogador);
		return jogador.getId();
	}
	
	public void atualizar(Jogador jogador) throws IOException {
		manager.merge(jogador);
	}
	
	public void excluir(int id) throws IOException {
		Query query = manager.createQuery("update Jogador j set j.ativo = false where j.id = :id");
		query.setParameter("id", id);
		query.executeUpdate();
	}
	
	public Jogador buscar(int id) throws IOException {
		Query query = manager.createQuery("select j from Jogador j where j.ativo = true and j.id = :id");
		query.setParameter("id", id);
		return (Jogador) query.getSingleResult();
	}
	
	@SuppressWarnings("unchecked")
	public List<Jogador> buscarTodos() {
		return manager.createQuery("select j from Jogador j where j.ativo = true order by j.id asc").getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Jogador> buscarPorCategoria(int id) {
		Query query = manager.createQuery("select j from Jogador j where j.ativo = true and j.disponivel = true and j.categoria.id = :id");
		query.setParameter("id", id);
		return query.getResultList();
	}
	
	public void ocupar(int id) {
		Query query = manager.createQuery("update Jogador j set j.disponivel = false where j.id = :id");
		query.setParameter("id", id);
		query.executeUpdate();
	}
	
	public void desocupar(int id) {
		Query query = manager.createQuery("update Jogador j set j.disponivel = true where j.id = :id");
		query.setParameter("id", id);
		query.executeUpdate();
	}
	
}
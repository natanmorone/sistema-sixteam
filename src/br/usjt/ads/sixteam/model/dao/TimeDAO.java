package br.usjt.ads.sixteam.model.dao;

import java.io.IOException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import br.usjt.ads.sixteam.model.entity.Time;

@Repository
public class TimeDAO {
	@PersistenceContext
	EntityManager manager;
	
	public int inserir(Time time) throws IOException {
		manager.persist(time);
		return time.getId();
	}
	
	public void atualizar(Time time) throws IOException {
		manager.merge(time);
	}
	
	public void excluir(int id) throws IOException {
		Query query = manager.createQuery("update Time t set t.ativo = false where t.id = :id");
		query.setParameter("id", id);
		query.executeUpdate();
	}
	
	public Time buscar(int id) throws IOException {
		return manager.find(Time.class, id);
	}
	
	@SuppressWarnings("unchecked")
	public boolean isTimeCadastrado(String nome) throws IOException {
		Query query = manager.createQuery("select t from Time t where t.nome = :nome");
		query.setParameter("nome", nome);
		List<Time> lista = query.getResultList();
		if (lista.size() > 0) {
			return true;
		}
		return false;
	}
	
	@SuppressWarnings("unchecked")
	public List<Time> buscarTodosPorCategoria(int id) throws IOException {
		Query query = manager.createQuery("select t from Time t where t.categoria.id = :id and t.ativo = true");
		query.setParameter("id", id);
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Time> buscarTodos() throws IOException {
		return manager.createQuery("select t from Time t where t.ativo = true order by t.id asc").getResultList();
	}
	
}
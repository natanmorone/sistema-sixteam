package br.usjt.ads.sixteam.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name="time")
public class Time {
	@Id
	@Column(name="id")
	@NotNull
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	@NotNull
	@Column(name="nome")
	@Size(min=5, max=45, message="Por favor digite um nome entre 5 a 45 caracteres")
	private String nome;
	@NotNull
	@Column(name="bandeira")
	@Size(min=5, max=45, message="Por favor digite um endere�o URL entre 5 a 45 caracteres")
	private String bandeira;
	@NotNull
	@ManyToOne
	@JoinColumn(name="id_categoria")
	private Categoria categoria;
	@NotNull
	@ManyToOne
	@JoinColumn(name="id_tecnico")
	private Tecnico tecnico;
	@Column(name="ativo")
	private boolean ativo = true;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getBandeira() {
		return bandeira;
	}
	public void setBandeira(String bandeira) {
		this.bandeira = bandeira;
	}
	public Categoria getCategoria() {
		return categoria;
	}
	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}
	public Tecnico getTecnico() {
		return tecnico;
	}
	public void setTecnico(Tecnico tecnico) {
		this.tecnico = tecnico;
	}
	public boolean isAtivo() {
		return ativo;
	}
	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}
	
	@Override
	public String toString() {
		return "Time [id=" + id + ", nome=" + nome + ", bandeira=" + bandeira + ", categoria=" + categoria
				+ ", tecnico=" + tecnico + ", ativo=" + ativo + "]";
	}
	
}

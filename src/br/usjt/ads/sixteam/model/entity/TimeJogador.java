package br.usjt.ads.sixteam.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="time_jogador")
public class TimeJogador {
	@Id
	@Column(name="id")
	@NotNull
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	@NotNull
	private int numero;
	private boolean capitao = false;
	private boolean titular = true;
	@NotNull
	@ManyToOne
	@JoinColumn(name="id_time")
	private TimeCampeonato time;
	@NotNull
	@ManyToOne
	@JoinColumn(name="id_jogador")
	@Valid
	private Jogador jogador;
	@NotNull
	@ManyToOne
	@JoinColumn(name="id_posicao")
	private Posicao posicao;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getNumero() {
		return numero;
	}
	public void setNumero(int numero) {
		this.numero = numero;
	}
	public boolean isCapitao() {
		return capitao;
	}
	public void setCapitao(boolean capitao) {
		this.capitao = capitao;
	}
	public boolean isTitular() {
		return titular;
	}
	public void setTitular(boolean titular) {
		this.titular = titular;
	}
	public TimeCampeonato getTime() {
		return time;
	}
	public void setTime(TimeCampeonato time) {
		this.time = time;
	}
	public Jogador getJogador() {
		return jogador;
	}
	public void setJogador(Jogador jogador) {
		this.jogador = jogador;
	}
	public Posicao getPosicao() {
		return posicao;
	}
	public void setPosicao(Posicao posicao) {
		this.posicao = posicao;
	}
	
	@Override
	public String toString() {
		return "TimeJogador [id=" + id + ", numero=" + numero + ", capitao=" + capitao + ", titular=" + titular
				+ ", time=" + time + ", jogador=" + jogador + ", posicao=" + posicao + "]";
	}
	
	
	
}

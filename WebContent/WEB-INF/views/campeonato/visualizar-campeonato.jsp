<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<html>
<head>
<c:import url="../head.jsp" />
</head>
<body>
	<!-- NAVBAR START -->
	<c:import url="../menu.jsp" />
	<!-- NAVBAR END -->
	<!-- Container -->
	<div class="container p-5 size">
		<div class="row">
			<div class="col mb-3">
				<h1 class="title text-center">${campeonato.nome}</h1>
				<div class="title-underline bg-success"></div>
				<p class="text-center font-italic font-weight-light my-3 display-6">Informações
					do campeonato</p>
			</div>
		</div>
		<c:forEach var="grupo" items="${grupos}">
			<div class="row mt-3">
				<div class="col">
					<div class="table-responsive-md d-flex justify-content-center">
						<table class="table table-header-unselectable text-center">
							<thead class="thead-dark">
								<tr>
									<th colspan="13">Grupo ${grupo.nome}</th>
								</tr>
								<tr>
									<th scope="col" style="min-width: 12rem;">Equipe</th>
									<th scope="col" data-toggle="tooltip" data-placement="top"
										title="Pontos Ganhos">PG</th>
									<th scope="col" data-toggle="tooltip" data-placement="top"
										title="Jogos">JG</th>
									<th scope="col" data-toggle="tooltip" data-placement="top"
										title="Vitórias">VT</th>
									<th scope="col" data-toggle="tooltip" data-placement="top"
										title="Empates">EM</th>
									<th scope="col" data-toggle="tooltip" data-placement="top"
										title="Derrotas">DT</th>
									<th scope="col" data-toggle="tooltip" data-placement="top"
										title="Gols Pró">GP</th>
									<th scope="col" data-toggle="tooltip" data-placement="top"
										title="Gols Contra">GC</th>
									<th scope="col" data-toggle="tooltip" data-placement="top"
										title="Saldo de Gols">SG</th>
									<th scope="col" data-toggle="tooltip" data-placement="top"
										title="Cartões Amarelos">CA</th>
									<th scope="col" data-toggle="tooltip" data-placement="top"
										title="Cartões Vermelhos">CV</th>
									<th scope="col" data-toggle="tooltip" data-placement="top"
										title="Pontos Disciplinares">PD</th>
								</tr>
							</thead>
							<tbody>
								<c:set var="timesCadastrados" scope="session" value="${0}" />
								<c:forEach var="equipe" items="${timesCampeonato}">
									<c:if test="${ equipe.grupo.id == grupo.id }">
										<c:set var="timesCadastrados" scope="session"
											value="${timesCadastrados+1}" />
										<tr class="table-align-middle">
											<td style="width: 80px;"><a
												href="/sixteam/equipes/visualizar/${equipe.id}"
												data-toggle="tooltip" data-placement="left"
												title="${equipe.time.nome}"> <img
													class="img-fluid img-flag p-0"
													src="/sixteam/${equipe.time.bandeira}" alt="">
											</a></td>
											<td>${equipe.pg}</td>
											<td>${equipe.jg}</td>
											<td>${equipe.vt}</td>
											<td>${equipe.em}</td>
											<td>${equipe.dt}</td>
											<td>${equipe.gp}</td>
											<td>${equipe.gc}</td>
											<td>${equipe.sg}</td>
											<td>${equipe.ca}</td>
											<td>${equipe.cv}</td>
											<td>${equipe.pd}</td>
										</tr>
									</c:if>
								</c:forEach>
								<c:if test="${timesCadastrados == 0}">
									<tr class="table-align-middle">
										<td colspan="13">Nenhum time cadastrado nesse grupo</td>
									</tr>
								</c:if>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</c:forEach>
	</div>
	<!-- Fim Container -->

	<!-- Modal Login -->
	<c:import url="../login-modal.jsp" />
	<!-- footer -->
	<footer id="footer" class="bg-dark py-3 flex-shrink-0">
		<div class="container">
			<div class="row flex-column align-items-center">
				<div class="col-6 text-center">
					<a href="#" class="btn"> <i
						class="fab fa-facebook fa-3x text-white m-2"></i>
					</a> <a href="#" class="btn"> <i
						class="fab fa-twitter fa-3x text-white m-2"></i>
					</a> <a href="#" class="btn"> <i
						class="fab fa-instagram fa-3x text-white m-2"></i>
					</a> <a href="#" class="btn"> <i
						class="fab fa-google-plus fa-3x text-white m-2"></i>
					</a>
				</div>
				<div class="col-6">
					<p class="text-center text-bold text-white">2018 © sixTeam</p>
				</div>
			</div>
		</div>
	</footer>
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
		integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
		integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
		crossorigin="anonymous"></script>
	<script
		src="https://unpkg.com/bootstrap-material-design@4.1.1/dist/js/bootstrap-material-design.js"
		integrity="sha384-CauSuKpEqAFajSpkdjv3z9t8E7RlpJ1UP0lKM/+NdtSarroVKu069AlsRPKkFBz9"
		crossorigin="anonymous"></script>
	
	<script>
		$(document).ready(function() {
			$('body').bootstrapMaterialDesign();
			//let params = new URLSearchParams(window.location.search);
			$('[data-toggle="tooltip"]').tooltip();
		});
	</script>
</body>
</html>
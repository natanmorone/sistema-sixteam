<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<html>
<head>
<c:import url="../head.jsp" />
</head>
<body>
	<!-- NAVBAR START -->
	<c:import url="../menu.jsp" />
	<!-- NAVBAR END -->
	<!-- Container -->
	<div class="container p-5 size">
		<div class="row">
			<div class="col mb-3">
				<h1 class="title text-center">Jogadores ${time.time.nome} ${time.time.categoria.nome}</h1>
				<div class="title-underline bg-success"></div>
				<p class="text-center font-italic font-weight-light my-3 display-6">Lista
					de jogadores</p>
			</div>
		</div>
		<div class="row my-4">
			<div
				class="col d-flex flex-column align-items-center justify-content-center my-2">
				<form action="" class="w-50 my-0 position-absolute search-form">
					<div class="input-group">
						<input type="text" class="form-control"
							placeholder="Buscar um jogador" aria-label="" aria-describedby="">
						<a href="#" class="">
							<div class="input-group-append">
								<i class="fas fa-search"></i>
							</div>
						</a>
					</div>
				</form>
				<c:if test="${usuario != null}" >
					<a id="btn-novo" href="/sixteam/registrar_jogador?time=${time.id}"
						class="align-self-end btn btn-success bmd-btn-icon active d-flex align-items-center justify-content-center mr-2"
						data-toggle="tooltip" data-placement="left" title="Registrar Jogador">
						<i class="material-icons">add</i>
					</a>
				</c:if>
			</div>
		</div>
		<div class="row">
			<div class="col">
				<div class="table-responsive-md">
					<table class="table text-center">
						<thead class="thead-dark">
							<tr>
								<th scope="col">#</th>
								<th scope="col">Foto</th>
								<th scope="col">Nome</th>
								<th scope="col">Camisa</th>
								<th scope="col">Equipe</th>
								<th scope="col">Categoria</th>
								<th scope="col">Posição</th>
								<th scope="col">Função</th>
								<th scope="col">Capitão</th>
								<th scope="col">Ações</th>
							</tr>
						</thead>
						<tbody>
							<c:if test="${not empty jogadoresEquipe}">
								<c:forEach var="timeJogador" items="${jogadoresEquipe}">
									<tr class="table-align-middle">
										<th scope="row">${timeJogador.id}</th>
										<td><img src="/sixteam/${timeJogador.jogador.foto}" alt="foto de jogador" class="img-fluid img-thumbnail" style="max-width:100px; max-height:100px;" /></td>
										<td><a href="/sixteam/jogadores/visualizar/${timeJogador.id}" class="text-success">${timeJogador.jogador.nome}</a></td>
										<td>${timeJogador.numero}</td>
										<td>${timeJogador.time.time.nome}</td>
										<td>${timeJogador.jogador.categoria.nome}</td>
										<td>${timeJogador.posicao.posicao}</td>
										<td>
											<c:if test="${ timeJogador.titular == true }">
												Titular
											</c:if>
											<c:if test="${ timeJogador.titular == false }">
												Reserva
											</c:if>
										</td>
										<td>
											<c:if test="${ timeJogador.capitao == true }">
												X
											</c:if>
										</td>
										<td><c:if test="${ usuario != null}">
												<a href="/sixteam/equipe/jogador/editar/${timeJogador.id}" class="btn btn-warning btn-sm">Editar</a>
												<button type="button" class="btn btn-danger btn-sm"
													data-toggle="modal" data-target="#modal-deletar"
													data-jogador="${timeJogador.id}">Excluir</button>
											</c:if></td>
									</tr>
								</c:forEach>
							</c:if>
							<c:if test="${empty jogadoresEquipe}">
								<tr class="table-align-middle">
									<td colspan="10">Nenhum jogador cadastrado nessa equipe</td>
								</tr>
							</c:if>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col text-center">
				<a href="#" onclick="history.go(-1)" class="btn btn-secondary btn-lg mt-3">Voltar</a>
			</div>
		</div>
	</div>
	<!-- Fim Container -->

	<!-- Modal Login -->
	<c:import url="../login-modal.jsp" />
	<!--  Fim Modal Login -->

	<!-- Modal Deletar -->
	<div class="modal fade" id="modal-deletar" tabindex="-1" role="dialog"
		aria-labelledby="modalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header text-center">
					<h4 class="modal-title" id="modalLabel">Excluir jogador da equipe</h4>
				</div>
				<div class="modal-body">Você realmente deseja excluir esse
					jogador dessa equipe?</div>
				<div class="modal-footer">
					<form action="/sixteam/equipe/excluir_de_equipe" method="post">
						<input type="hidden" name="id" id="id_excluir" />
						<button type="submit" class="btn btn-danger">Sim</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">N&atilde;o</button>
					</form>
				</div>
			</div>
		</div>
	</div>
	<!-- Fim Modal Deletar -->
	<!-- footer -->
	<footer id="footer" class="bg-dark py-3 flex-shrink-0">
		<div class="container">
			<div class="row flex-column align-items-center">
				<div class="col-6 text-center">
					<a href="#" class="btn"> <i
						class="fab fa-facebook fa-3x text-white m-2"></i>
					</a> <a href="#" class="btn"> <i
						class="fab fa-twitter fa-3x text-white m-2"></i>
					</a> <a href="#" class="btn"> <i
						class="fab fa-instagram fa-3x text-white m-2"></i>
					</a> <a href="#" class="btn"> <i
						class="fab fa-google-plus fa-3x text-white m-2"></i>
					</a>
				</div>
				<div class="col-6">
					<p class="text-center text-bold text-white">2018 © sixTeam</p>
				</div>
			</div>
		</div>
	</footer>
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
		integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
		integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
		crossorigin="anonymous"></script>
	<script
		src="https://unpkg.com/bootstrap-material-design@4.1.1/dist/js/bootstrap-material-design.js"
		integrity="sha384-CauSuKpEqAFajSpkdjv3z9t8E7RlpJ1UP0lKM/+NdtSarroVKu069AlsRPKkFBz9"
		crossorigin="anonymous"></script>
	
	<script>
		$(document).ready(function() {
			$('body').bootstrapMaterialDesign();
			//let params = new URLSearchParams(window.location.search);
			$('[data-toggle="tooltip"]').tooltip();

			$("#modal-deletar").on('show.bs.modal', function(event) {
				var button = $(event.relatedTarget);
				var recipient = button.data('jogador');
				$("#id_excluir").val(recipient);
			});
		});
	</script>
</body>
</html>
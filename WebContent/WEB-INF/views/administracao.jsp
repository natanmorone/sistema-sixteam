<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<c:import url="head.jsp" />
</head>
<body>
	<!-- NAVBAR START -->
	<c:import url="menu.jsp" />
	<!-- NAVBAR END -->
	<!-- Container -->
	<div class="container p-5 size">
		<div class="row">
			<div class="col mb-5">
				<h1 class="title text-center">Administração</h1>
				<div class="title-underline bg-success"></div>
			</div>
		</div>
		<div class="row no-gutters">
			<div class="col-sm-6 col-md-4 col-lg-3 m-0">
				<a class="card-link" href="campeonatos">
					<div class="card">
						<div class="card-body text-center p-5">
							<i class="fas fa-trophy fa-5x text-dark"></i>
						</div>
						<div
							class="card-footer d-flex justify-content-center align-items-center py-2">
							<p class="text-dark m-0">Gerenciar Campeonatos</p>
						</div>
					</div>
				</a>
			</div>
			<div class="col-sm-6 col-md-4 col-lg-3 m-0">
				<a class="card-link" href="equipes">
					<div class="card">
						<div class="card-body text-center p-5">
							<i class="fas fa-flag-checkered fa-5x text-dark"></i>
						</div>
						<div
							class="card-footer d-flex justify-content-center align-items-center py-2">
							<p class="text-dark m-0">Gerenciar Equipes</p>
						</div>
					</div>
				</a>
			</div>
			<div class="col-sm-6 col-md-4 col-lg-3 m-0">
				<a class="card-link" href="jogadores">
					<div class="card">
						<div class="card-body text-center p-5">
							<i class="fas fa-users fa-5x text-dark"></i>
						</div>
						<div
							class="card-footer d-flex justify-content-center align-items-center py-2">
							<p class="text-dark m-0">Gerenciar Jogadores</p>
						</div>
					</div>
				</a>
			</div>
			<div class="col-sm-6 col-md-4 col-lg-3 m-0">
				<a class="card-link" href="/sixteam/selecao_campeonato?tipo=jogos">
					<div class="card">
						<div class="card-body text-center p-5">
							<i class="fas fa-futbol fa-5x text-dark"></i>
						</div>
						<div
							class="card-footer d-flex justify-content-center align-items-center py-2">
							<p class="text-dark m-0">Gerenciar Jogos</p>
						</div>
					</div>
				</a>
			</div>
			<div class="col-sm-6 col-md-4 col-lg-3 m-0">
				<a class="card-link" href="treinadores">
					<div class="card">
						<div class="card-body text-center p-5">
							<i class="fas fa-user fa-5x text-dark"></i>
						</div>
						<div
							class="card-footer d-flex justify-content-center align-items-center py-2">
							<p class="text-dark m-0">Gerenciar Treinadores</p>
						</div>
					</div>
				</a>
			</div>
			<div class="col-sm-6 col-md-4 col-lg-3 m-0">
				<a class="card-link" href="arbitros">
					<div class="card">
						<div class="card-body text-center p-5">
							<i class="far fa-user fa-5x text-dark"></i>
						</div>
						<div
							class="card-footer d-flex justify-content-center align-items-center py-2">
							<p class="text-dark m-0">Gerenciar Árbitros</p>
						</div>
					</div>
				</a>
			</div>
			<div class="col-sm-6 col-md-4 col-lg-3 m-0">
				<a class="card-link" href="locais">
					<div class="card">
						<div class="card-body text-center p-5">
							<i class="fas fa-kaaba fa-5x text-dark"></i>
						</div>
						<div
							class="card-footer d-flex justify-content-center align-items-center py-2">
							<p class="text-dark m-0">Gerenciar Locais</p>
						</div>
					</div>
				</a>
			</div>
			<div class="col-sm-6 col-md-4 col-lg-3 m-0">
				<a class="card-link" href="gerar_dados">
					<div class="card">
						<div class="card-body text-center p-5">
							<i class="far fa-newspaper fa-5x text-dark"></i>
						</div>
						<div
							class="card-footer d-flex justify-content-center align-items-center py-2">
							<p class="text-dark m-0">Gerar Relatórios</p>
						</div>
					</div>
				</a>
			</div>
		</div>
	</div>
	<!-- Fim Container -->

	<!-- Modal Login -->
	<c:import url="login-modal.jsp" />
	<!-- footer -->
	<footer id="footer" class="bg-dark py-3 flex-shrink-0">
		<div class="container">
			<div class="row flex-column align-items-center">
				<div class="col-6 text-center">
					<a href="#" class="btn"> <i
						class="fab fa-facebook fa-3x text-white m-2"></i>
					</a> <a href="#" class="btn"> <i
						class="fab fa-twitter fa-3x text-white m-2"></i>
					</a> <a href="#" class="btn"> <i
						class="fab fa-instagram fa-3x text-white m-2"></i>
					</a> <a href="#" class="btn"> <i
						class="fab fa-google-plus fa-3x text-white m-2"></i>
					</a>
				</div>
				<div class="col-6">
					<p class="text-center text-bold text-white">2018 © sixTeam</p>
				</div>
			</div>
		</div>
	</footer>
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
		integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
		integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
		crossorigin="anonymous"></script>
	<script
		src="https://unpkg.com/bootstrap-material-design@4.1.1/dist/js/bootstrap-material-design.js"
		integrity="sha384-CauSuKpEqAFajSpkdjv3z9t8E7RlpJ1UP0lKM/+NdtSarroVKu069AlsRPKkFBz9"
		crossorigin="anonymous"></script>
	<script>
		$(document).ready(function() {
			$('body').bootstrapMaterialDesign();
			//let params = new URLSearchParams(window.location.search);
		});
	</script>
</body>
</html>
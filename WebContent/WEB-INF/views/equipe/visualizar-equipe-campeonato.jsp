<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<html>
<head>
<c:import url="../head.jsp" />
</head>
<body>
	<!-- NAVBAR START -->
	<c:import url="../menu.jsp" />
	<!-- NAVBAR END -->
	<!-- Container -->
	<div class="container py-5 size">
		<div class="row justify-content-center">
			<div class="col-2">
				<div class="card card-bandeira-equipe">
					<img class="card-img img-fluid"
						src="/sixteam/${timeCampeonato.time.bandeira}"
						alt="">
					<div class="card-footer text-center">
						<h5 class="my-0">${timeCampeonato.time.nome}</h5>
					</div>
				</div>
			</div>
			<div class="col-5">
				<div class="card">
					<div class="card-header text-center py-2">
						<h5 class="card-title my-0">Titulares</h5>
					</div>
					<div class="card-body">
						<div class="row">
							<div class="table-responsive">
								<table class="table table-borderless text-center">
									<thead>
										<tr>
											<th>Nº</th>
											<th>Nome</th>
											<th>Posição</th>
										</tr>
									</thead>
									<tbody>
										<c:if test="${not empty titulares}">
											<c:forEach var="timeJogador" items="${titulares}">
												<tr>
													<td>${timeJogador.numero}</td>
													<td><a class="text-success" href="/sixteam/jogadores/visualizar/${timeJogador.id}">${timeJogador.jogador.nome}</a></td>
													<td class="font-weight-bold">${timeJogador.posicao.posicao}</td>
												</tr>
											</c:forEach>
										</c:if>
										<c:if test="${empty titulares}">
											<tr class="table-align-middle">
												<td colspan="4">Nenhum jogador titular cadastrado</td>
											</tr>
										</c:if>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-5">
				<div class="card">
					<div class="card-header text-center py-2">
						<h5 class="card-title my-0">Reservas</h5>
					</div>
					<div class="card-body">
						<div class="row">
							<div class="table-responsive">
								<table class="table table-borderless text-center">
									<thead>
										<tr>
											<th>Nº</th>
											<th>Nome</th>
											<th>Posição</th>
										</tr>
									</thead>
									<tbody>
										<c:if test="${not empty reservas}">
											<c:forEach var="timeJogador" items="${reservas}">
												<tr>
													<td>${timeJogador.numero}</td>
													<td><a class="text-success" href="/sixteam/jogadores/visualizar/${timeJogador.id}">${timeJogador.jogador.nome}</a></td>
													<td class="font-weight-bold">${timeJogador.posicao.posicao}</td>
												</tr>
											</c:forEach>
										</c:if>
										<c:if test="${empty reservas}">
											<tr class="table-align-middle">
												<td colspan="4">Nenhum jogador reserva cadastrado</td>
											</tr>
										</c:if>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col">
				<div class="row mt-5">
					<div class="col">
						<h1 class="title text-center">Jogos</h1>
						<div class="title-underline bg-success"></div>
					</div>
				</div>
				<div class="row mt-2 justify-content-center">
					<div class="table-responsive-md mt-3 w-100">
						<table class="table text-center">
							<thead class="thead-dark">
								<tr>
									<th scope="col">#</th>
									<th scope="col">Data</th>
									<th scope="col">Hora</th>
									<th scope="col">Local</th>
									<th scope="col">Árbitro</th>
									<th scope="col"><i class="fas fa-eye fa-1x"></i></th>
								</tr>
							</thead>
							<tbody>
								<c:if test="${not empty listaJogos}">
									<c:forEach var="jogo" items="${listaJogos}">
										<tr class="table-align-middle">
											<td>${jogo.id}</td>
											<td>
												<fmt:formatDate value="${jogo.data}" pattern="dd/MM/yyyy"/>
											</td>
											<td>
												<fmt:formatDate value="${jogo.hora}" pattern="HH:mm"/>
											</td>
											<td>${jogo.local.nome}</td>
											<td>${jogo.arbitro.nome}</td>
											<td><a href="/sixteam/jogos/visualizar/${jogo.id}" class="btn btn-success">Visualizar</a></td>
										</tr>
									</c:forEach>
								</c:if>
								<c:if test="${empty listaJogos}">
									<tr class="table-align-middle">
										<td colspan="6">Equipe não está cadastrada em nenhum jogo</td>
									</tr>
								</c:if>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col text-center">
				<a href="#" onclick="history.go(-1)" class="btn btn-secondary btn-lg mt-3">Voltar</a>
			</div>
		</div>
	</div>
	<!-- Fim Container -->

	<!-- Modal Login -->
	<c:import url="../login-modal.jsp" />
	<!-- footer -->
	<footer id="footer" class="bg-dark py-3 flex-shrink-0">
		<div class="container">
			<div class="row flex-column align-items-center">
				<div class="col-6 text-center">
					<a href="#" class="btn"> <i
						class="fab fa-facebook fa-3x text-white m-2"></i>
					</a> <a href="#" class="btn"> <i
						class="fab fa-twitter fa-3x text-white m-2"></i>
					</a> <a href="#" class="btn"> <i
						class="fab fa-instagram fa-3x text-white m-2"></i>
					</a> <a href="#" class="btn"> <i
						class="fab fa-google-plus fa-3x text-white m-2"></i>
					</a>
				</div>
				<div class="col-6">
					<p class="text-center text-bold text-white">2018 © sixTeam</p>
				</div>
			</div>
		</div>
	</footer>
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
		integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
		integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
		crossorigin="anonymous"></script>
	<script
		src="https://unpkg.com/bootstrap-material-design@4.1.1/dist/js/bootstrap-material-design.js"
		integrity="sha384-CauSuKpEqAFajSpkdjv3z9t8E7RlpJ1UP0lKM/+NdtSarroVKu069AlsRPKkFBz9"
		crossorigin="anonymous"></script>
	
	<script>
		$(document).ready(function() {
			$('body').bootstrapMaterialDesign();
			//let params = new URLSearchParams(window.location.search);
		});
	</script>
</body>
</html>
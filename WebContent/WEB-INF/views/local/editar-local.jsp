<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<c:import url="../head.jsp" />
</head>
<body>
	<!-- NAVBAR START -->
	<c:import url="../menu.jsp" />
	<!-- NAVBAR END -->
	<!-- Container -->
	<div class="container p-5 size">
		<div class="row">
			<div class="col mb-3">
				<h1 class="title text-center">Editar Local ${local.nome}</h1>
				<div class="title-underline bg-success"></div>
				<p class="text-center font-italic font-weight-light my-3 display-6">Informe
					os dados do local</p>
			</div>
		</div>
		<div class="row">
			<div class="col">
				<div class="erros d-flex justify-content-center align-items-center flex-column">
					<form:errors path="local.nome" cssStyle="color:red;font-size:14px"/>
				</div>
				<form action="/sixteam/atualizar_local" method="post">
					<input type="hidden" name="id" value="${local.id}" />
					<div class="form-row">
						<div class="col">
							<div class="form-group">
								<label for="nome">Nome do Local</label> <input type="text"
									id="nome" name="nome" value="${local.nome}" class="form-control">
							</div>
						</div>
					</div>
					<div class="form-row">
						<div class="col d-flex justify-content-center">
							<button type="submit" class="btn btn-success btn-lg mt-3">Salvar</button>
							<a href="#" onclick="history.go(-1)" class="btn btn-secondary btn-lg mt-3">Cancelar</a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- Fim Container -->

	<!-- Modal Login -->
	<c:import url="../login-modal.jsp" />
	<!-- footer -->
	<footer id="footer" class="bg-dark py-3 flex-shrink-0">
		<div class="container">
			<div class="row flex-column align-items-center">
				<div class="col-6 text-center">
					<a href="#" class="btn"> <i
						class="fab fa-facebook fa-3x text-white m-2"></i>
					</a> <a href="#" class="btn"> <i
						class="fab fa-twitter fa-3x text-white m-2"></i>
					</a> <a href="#" class="btn"> <i
						class="fab fa-instagram fa-3x text-white m-2"></i>
					</a> <a href="#" class="btn"> <i
						class="fab fa-google-plus fa-3x text-white m-2"></i>
					</a>
				</div>
				<div class="col-6">
					<p class="text-center text-bold text-white">2018 © sixTeam</p>
				</div>
			</div>
		</div>
	</footer>
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
		integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
		integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
		crossorigin="anonymous"></script>
	<script
		src="https://unpkg.com/bootstrap-material-design@4.1.1/dist/js/bootstrap-material-design.js"
		integrity="sha384-CauSuKpEqAFajSpkdjv3z9t8E7RlpJ1UP0lKM/+NdtSarroVKu069AlsRPKkFBz9"
		crossorigin="anonymous"></script>
	
	<script>
		$(document).ready(function() {
			$('body').bootstrapMaterialDesign();
			//let params = new URLSearchParams(window.location.search);
		});
	</script>
</body>
</html>
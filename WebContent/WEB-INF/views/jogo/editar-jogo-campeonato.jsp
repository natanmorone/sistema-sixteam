<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html>
<head>
<c:import url="../head.jsp" />
</head>
<body>
	<!-- NAVBAR START -->
	<c:import url="../menu.jsp" />
	<!-- NAVBAR END -->
	<!-- Container -->
	<div class="container p-5 size">
		<div class="row">
			<div class="col mb-3">
				<h1 class="title text-center">Editar Jogo #${jogo.id}</h1>
				<div class="title-underline bg-success"></div>
				<p class="text-center font-italic font-weight-light my-3 display-6">Informe
					os dados do jogo</p>
			</div>
		</div>
		<div class="row">
			<div class="col">
				<form action="/sixteam/atualizar_jogo_campeonato" method="post">
					<input type="hidden" name="id" value="${jogo.id}" />
					<input type="hidden" name="campeonato.id" value="${campeonato.id}" />
					<div class="form-row">
						<div class="col">
							<div class="form-group">
								<label for="local">Local da Partida</label> 
								<select id="local" class="form-control" name="local.id" style="height: 2.076rem;">
									<c:forEach var="local" items="${locais}">
										<c:if test="${ local.id == jogo.local.id }">
											<option value="${local.id}" selected>${local.nome}</option>
										</c:if>
										<c:if test="${ local.id != jogo.local.id }">
											<option value="${local.id}">${local.nome}</option>
										</c:if>
									</c:forEach>
								</select>
							</div>
						</div>
						<div class="col">
							<div class="form-group">
								<label for="arbitro">Árbitro</label>
								<select id="arbitro" name="arbitro.id" class="form-control" style="height: 2.076rem;">
									<c:forEach var="arbitro" items="${arbitros}">
										<c:if test="${ arbitro.id == jogo.arbitro.id }">
											<option value="${arbitro.id}" selected>${arbitro.nome}</option>
										</c:if>
										<c:if test="${ arbitro.id != jogo.arbitro.id }">
											<option value="${arbitro.id}">${arbitro.nome}</option>
										</c:if>
									</c:forEach>
								</select>
							</div>
						</div>
					</div>
					<div class="form-row">
						<div class="col">
							<div class="form-group">
								<label for="timeCasa">Time da Casa</label>
								<select id="timeCasa" name="timeCasa.id" class="form-control" style="height: 2rem;">
									<c:forEach var="timeCasa" items="${times}">
										<c:if test="${ timeCasa.id == jogo.timeCasa.id }">
											<option value="${timeCasa.id}" selected>${timeCasa.nome}</option>
										</c:if>
										<c:if test="${ timeCasa.id != jogo.timeCasa.id }">
											<option value="${timeCasa.id}">${timeCasa.nome}</option>
										</c:if>
									</c:forEach>
								</select>
							</div>
						</div>
						<div class="col">
							<div class="form-group">
								<label for="timeVisitante">Time Visitante</label>
								<select id="timeVisitante" name="timeVisitante.id" class="form-control" style="height: 2rem;">
									<c:forEach var="timeVisitante" items="${times}">
										<c:if test="${ timeVisitante.id == jogo.timeVisitante.id }">
											<option value="${timeVisitante.id}" selected>${timeVisitante.nome}</option>
										</c:if>
										<c:if test="${ timeVisitante.id != jogo.timeVisitante.id }">
											<option value="${timeVisitante.id}">${timeVisitante.nome}</option>
										</c:if>
									</c:forEach>
								</select>
							</div>
						</div>
					</div>
					<div class="form-row">
						<div class="col">
							<div class="form-group">
								<label for="turno">Turno</label>
								<select id="turno" name="turno" class="form-control" style="height: 2rem;">
									<c:forTokens var="turno" items="1,2,8ª de Final,4ª de Final,Semi-Final,Final" delims="," varStatus="t">
										<c:if test="${ jogo.turno == t.index+1 }">
											<option value="${t.index+1}" selected>${turno}</option>
										</c:if>
										<c:if test="${ jogo.turno != t.index+1 }">
											<option value="${t.index+1}">${turno}</option>
										</c:if>
									</c:forTokens>
								</select>
							</div>
						</div>
						<div class="col">
							<div class="form-group">
								<label for="rodada">Rodada</label>
								<select id="rodada" name="rodada" class="form-control" style="height: 2rem;">
									<c:forEach var="r" begin="1" end="3">
										<c:if test="${ r == jogo.rodada }">
											<option value="${r}" selected>${r}</option>
										</c:if>
										<c:if test="${ r != jogo.rodada }">
											<option value="${r}">${r}</option>
										</c:if>
									</c:forEach>
								</select>
							</div>
						</div>
					</div>
					<div class="form-row">
						<div class="col">
							<div class="form-group">
								<fmt:formatDate value="${jogo.data}" type="date" pattern="dd/MM/yyyy" var="dataFormatada" />
								<label for="data">Data da Partida</label>
								<input id="data" name="data" class="form-control" onKeyPress="return false;" value="${dataFormatada}" />
							</div>
						</div>
						<div class="col">
							<div class="form-group">
								<fmt:formatDate value="${jogo.hora}" type="time" pattern="hh:mm" var="horaFormatada" />
								<label for="hora">Horário da Partida</label>
								<input id="hora" name="horaDaPartida" class="form-control" onKeyPress="return false;" value="${horaFormatada}" />
							</div>
						</div>
					</div>
					<div class="form-row">
						<div class="col d-flex justify-content-center">
							<button type="submit" class="btn btn-success btn-lg mt-3">Salvar</button>
							<a href="#" onclick="history.go(-1)" class="btn btn-secondary btn-lg mt-3">Cancelar</a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- Fim Container -->

	<!-- Modal Login -->
	<c:import url="../login-modal.jsp" />
	<!-- footer -->
	<footer id="footer" class="bg-dark py-3 flex-shrink-0">
		<div class="container">
			<div class="row flex-column align-items-center">
				<div class="col-6 text-center">
					<a href="#" class="btn"> <i
						class="fab fa-facebook fa-3x text-white m-2"></i>
					</a> <a href="#" class="btn"> <i
						class="fab fa-twitter fa-3x text-white m-2"></i>
					</a> <a href="#" class="btn"> <i
						class="fab fa-instagram fa-3x text-white m-2"></i>
					</a> <a href="#" class="btn"> <i
						class="fab fa-google-plus fa-3x text-white m-2"></i>
					</a>
				</div>
				<div class="col-6">
					<p class="text-center text-bold text-white">2018 © sixTeam</p>
				</div>
			</div>
		</div>
	</footer>
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
		integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
		integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
		crossorigin="anonymous"></script>
	<script
		src="https://unpkg.com/bootstrap-material-design@4.1.1/dist/js/bootstrap-material-design.js"
		integrity="sha384-CauSuKpEqAFajSpkdjv3z9t8E7RlpJ1UP0lKM/+NdtSarroVKu069AlsRPKkFBz9"
		crossorigin="anonymous"></script>
	
	<!-- Datepicker JS -->
	<script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.10/js/gijgo.min.js"
		type="text/javascript"></script>
	<script>
		$(document).ready(function() {
			$('body').bootstrapMaterialDesign();
			//let params = new URLSearchParams(window.location.search);
			$('[data-toggle="tooltip"]').tooltip();

			$('#data').datepicker({
		        showOtherMonths: true,
		        format: 'dd/mm/yyyy'
		    });
		    $('#hora').timepicker();
		});
	</script>
</body>
</html>
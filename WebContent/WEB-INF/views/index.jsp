<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<c:import url="head.jsp" />
</head>
<body>
	<!-- NAVBAR START -->
	<c:import url="menu.jsp" />
	<!-- NAVBAR END -->
	<!-- Container -->
	<div id="index-container" class="container size">
		<div class="row align-items-center">
			<div class="col-md-6 p-4">
				<h1 class="title text-center">Futebol dos Pais</h1>
				<div class="title-underline bg-success"></div>
				<p class="mt-4 text-justify">Lorem ipsum dolor sit amet Lorem
					ipsum dolor sit amet, consectetur adipisicing elit. Dolores modi
					earum quam quas quo eaque at, alias aperiam, omnis nihil maiores
					iste accusantium voluptas optio odit iusto perferendis, mollitia
					impedit! Ex porro magnam non tempore ullam eaque delectus, adipisci
					perspiciatis minima accusamus est, debitis! Eius nihil obcaecati
					culpa vitae autem placeat laborum neque reprehenderit aliquam
					consectetur, dolore rem eos, et doloribus quisquam, eveniet ullam
					saepe? Assumenda sit autem odio libero ab rerum natus numquam
					corporis soluta expedita omnis dolor illum explicabo illo modi
					molestias, minus laborum architecto, amet laudantium voluptatibus
					quod quo. Hic dolore est asperiores, accusantium, laboriosam eos
					iure., consectetur adipisicing elit. Impedit reprehenderit magni
					eligendi fugit possimus adipisci sapiente cum nulla sunt aliquam,
					veritatis sed fuga, dolorem dicta velit maxime rerum pariatur quas.
					Sint debitis at adipisci assumenda omnis architecto, veritatis
					laboriosam quos dolorem. Dolorem ad doloremque, id tempore
					voluptate voluptatibus velit enim?</p>
			</div>
			<div class="col-md-6 p-4">
				<img src="img/inicio-1.jpg" class="img-fluid rounded img-thumbnail"
					alt="Mais de 50 campeonatos por ano">
				<p class="text-center mt-1">Mais de 50 campeonatos por ano</p>
			</div>
			<div class="col-md-6 p-4">
				<img src="img/inicio-2.jpg" class="img-fluid rounded img-thumbnail"
					alt="Cameponato feminino de futebol">
				<p class="text-center mt-1">Campeonato feminino de futebol</p>
			</div>
			<div class="col-md-6 p-4">
				<h1 class="title text-center">Gerenciamento de Campeonatos</h1>
				<div class="title-underline bg-success"></div>
				<p class="mt-4 text-justify">Lorem ipsum dolor sit amet,
					consectetur adipisicing elit. Eligendi rerum explicabo unde, dicta
					blanditiis harum obcaecati voluptatum porro repellendus asperiores
					fugit temporibus distinctio vero debitis aliquid odio sint incidunt
					nam. Delectus quaerat debitis laborum, ut!</p>
			</div>
		</div>
	</div>
	<!-- Fim Container -->
	
	<!-- Modal Login -->
	<c:import url="login-modal.jsp"/>
	<!-- footer -->
	<footer id="footer" class="bg-dark py-3 flex-shrink-0">
		<div class="container">
			<div class="row flex-column align-items-center">
				<div class="col-6 text-center">
					<a href="#" class="btn"> <i
						class="fab fa-facebook fa-3x text-white m-2"></i>
					</a> <a href="#" class="btn"> <i
						class="fab fa-twitter fa-3x text-white m-2"></i>
					</a> <a href="#" class="btn"> <i
						class="fab fa-instagram fa-3x text-white m-2"></i>
					</a> <a href="#" class="btn"> <i
						class="fab fa-google-plus fa-3x text-white m-2"></i>
					</a>
				</div>
				<div class="col-6">
					<p class="text-center text-bold text-white">2018 © sixTeam</p>
				</div>
			</div>
		</div>
	</footer>
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
		integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
		crossorigin="anonymous"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
		integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
		crossorigin="anonymous"></script>
	<script
		src="https://unpkg.com/bootstrap-material-design@4.1.1/dist/js/bootstrap-material-design.js"
		integrity="sha384-CauSuKpEqAFajSpkdjv3z9t8E7RlpJ1UP0lKM/+NdtSarroVKu069AlsRPKkFBz9"
		crossorigin="anonymous"></script>
	<script>
		$(document).ready(function() {
			$('body').bootstrapMaterialDesign();
			//let params = new URLSearchParams(window.location.search);
		});
	</script>
</body>
</html>